/**
 *
 * UiSpacer
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';

function UiSpacer({ ...props }) {
  return <div {...props}>&nbsp;</div>;
}

UiSpacer.propTypes = {};

export default memo(UiSpacer);
