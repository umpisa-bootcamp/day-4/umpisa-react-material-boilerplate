import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(theme => ({
  root: {
    padding: 0,
    overflow: 'hidden', // if not radius will not work
  },
  title: {
    color: '#000',
    paddingLeft: theme.spacing(2),
    paddingTop: theme.spacing(1),
    paddingRight: theme.spacing(4),
    paddingBottom: theme.spacing(1),
  },
  content: {
    padding: theme.spacing(3),
  },
}));
