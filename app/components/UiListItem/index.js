/**
 *
 * UiListItem
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { useStyles } from './style';

function UiListItem({ title, children }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.title}>{title}</div>
      <div className={classes.content}>{children}</div>
    </div>
  );
}

UiListItem.propTypes = {
  title: PropTypes.any,
  children: PropTypes.any,
};

export default memo(UiListItem);
