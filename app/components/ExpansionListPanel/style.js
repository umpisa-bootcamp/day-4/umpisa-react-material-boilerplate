import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
  summary: {
    fontWeight: 500,
    fontSize: '24px',
    lineHeight: '28px',
    letterSpacing: '0.15px',
    color: '#244E60',
  },
  title: {
    fontWeight: 'normal',
    fontSize: '16px',
    lineHeight: '19px',
    letterSpacing: '0.15px',
    color: '#333333',
  },
}));
