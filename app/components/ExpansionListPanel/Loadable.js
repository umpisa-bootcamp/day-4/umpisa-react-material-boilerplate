/**
 *
 * Asynchronously loads the component for ExpansionListPanel
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
