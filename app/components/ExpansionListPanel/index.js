/**
 *
 * ExpansionListPanel
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  List,
  ListItem,
  Typography,
} from '@material-ui/core';
import expandIcon from '../../images/expand.svg';

import { useStyles } from './style';
function ExpansionListPanel({ history, items = [], summary }) {
  const classes = useStyles();

  return (
    <ExpansionPanel>
      <ExpansionPanelSummary
        expandIcon={<img src={expandIcon} alt="expand" />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.summary}>{summary}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <List dense style={{ width: '100%' }}>
          {items.map(i => (
            <ListItem
              button
              onClick={() => history.push(`/help-center/${i.id}`)}
            >
              <Typography className={classes.title}>{i.title}</Typography>
            </ListItem>
          ))}
        </List>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

ExpansionListPanel.propTypes = {
  items: PropTypes.array,
  summary: PropTypes.string.isRequired,
  history: PropTypes.object,
};

export default memo(ExpansionListPanel);
