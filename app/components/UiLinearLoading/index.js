/**
 *
 * UiLinearLoading
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { LinearProgress } from '@material-ui/core';
import ConditionalDisplay from '../ConditionalDisplay';

function UiLinearLoading({ isLoading }) {
  return (
    <ConditionalDisplay visible={isLoading}>
      <LinearProgress />
    </ConditionalDisplay>
  );
}

UiLinearLoading.propTypes = {
  isLoading: PropTypes.bool,
};

export default memo(UiLinearLoading);
