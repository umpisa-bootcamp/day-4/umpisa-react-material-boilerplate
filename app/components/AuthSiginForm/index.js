/**
 *
 * AuthSiginForm
 *
 */

import React, { memo, useState, useCallback } from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  FormControl,
  FormHelperText,
  Grid,
  TextField,
} from '@material-ui/core';
import { useStyles } from './style';
import Logo from '../Logo';
import ThemeDark from '../ThemeDark';
import PasswordVisibility from './PasswordVisibility';

function AuthSiginForm({ onLogin, isLoading, error }) {
  const classes = useStyles();
  const [state, setState] = useState({
    username: '',
    password: '',
    showPassword: false,
  });

  const onSubmit = useCallback(e => {
    e.preventDefault();
    onLogin(state.username, state.password);
  });

  const handleClickShowPassword = () => {
    setState({ ...state, showPassword: !state.showPassword });
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  return (
    <ThemeDark>
      <Grid container justify="center" className={classes.background}>
        <Grid item>
          <Logo auth position />
        </Grid>
        <Grid item container justify="center">
          <form onSubmit={onSubmit}>
            <TextField
              fullWidth
              id="name"
              margin="normal"
              label="Email address"
              onChange={e => setState({ ...state, username: e.target.value })}
              type="email"
              variant="outlined"
              required
            />
            <FormControl fullWidth>
              <TextField
                fullWidth
                id="password"
                margin="normal"
                label="Password"
                required
                onChange={e => setState({ ...state, password: e.target.value })}
                InputProps={{
                  endAdornment: (
                    <PasswordVisibility
                      show={state.showPassword}
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                    />
                  ),
                }}
                type={state.showPassword ? 'text' : 'password'}
                variant="outlined"
              />
            </FormControl>
            <FormHelperText
              className={`${classes.error} ${error ? classes.onError : ''}`}
              error
              margin="dense"
            >
              {error}&nbsp;
            </FormHelperText>
            <Grid alignItems="center" container justify="space-between">
              <Grid item xs={12} sm={6}>
                <Button
                  disabled={isLoading}
                  size="medium"
                  variant="contained"
                  type="submit"
                  className={classes.loginButton}
                >
                  LOG IN
                </Button>
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                container
                justify="flex-end"
                className={classes.forgotPasswordContainer}
              >
                <Button className={classes.forgotPassword}>
                  Forgot Password?
                </Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    </ThemeDark>
  );
}

AuthSiginForm.propTypes = {
  error: PropTypes.string,
  isLoading: PropTypes.bool.isRequired,
  onLogin: PropTypes.func.isRequired,
};

export default memo(AuthSiginForm);
