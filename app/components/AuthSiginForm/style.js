import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  error: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  input: {
    display: 'none',
  },
  forgotPassword: {
    textDecoration: 'underline',
    fontWeight: 100,
    textTransform: 'capitalize',
  },
  loginButton: {
    [theme.breakpoints.only('xs', 'sm')]: {
      width: '100%',
    },
  },
  background: {
    [theme.breakpoints.only('xs', 'sm')]: {
      paddingLeft: 20,
      paddingRight: 20,
    },
  },
  forgotPasswordContainer: {
    [theme.breakpoints.only('xs', 'sm')]: {
      marginTop: 20,
      justifyContent: 'center',
    },
  },

  onError: {
    color: '#FF9259 !important',
  },
}));
