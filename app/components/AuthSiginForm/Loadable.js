/**
 *
 * Asynchronously loads the component for AuthSiginForm
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
