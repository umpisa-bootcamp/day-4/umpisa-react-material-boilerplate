/**
 *
 * AuthSiginForm
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { IconButton, InputAdornment } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

function PasswordVisibility({ onClick, onMouseDown, show }) {
  return (
    <InputAdornment position="end">
      <IconButton
        aria-label="toggle password visibility"
        onClick={onClick}
        onMouseDown={onMouseDown}
      >
        {show ? <Visibility /> : <VisibilityOff />}
      </IconButton>
    </InputAdornment>
  );
}

PasswordVisibility.propTypes = {
  show: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  onMouseDown: PropTypes.func.isRequired,
};

export default memo(PasswordVisibility);
