import { makeStyles } from '@material-ui/core/styles';
import { colors } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  root: {
    textTransform: 'uppercase',
    fontSize: 10,
    color: colors.grey[700],
  },
}));
