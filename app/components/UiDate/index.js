/**
 *
 * UiDate
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import moment from 'moment';
import { useStyles } from './style';
function UiDate({ date }) {
  const classes = useStyles();
  return (
    <Typography variant="caption" className={classes.root}>
      {moment(date).format('MMMM D, YYYY, H:MM:A')}
    </Typography>
  );
}

UiDate.propTypes = {
  date: PropTypes.number.isRequired,
};

export default memo(UiDate);
