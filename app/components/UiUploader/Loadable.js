/**
 *
 * Asynchronously loads the component for UiUploader
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
