/**
 *
 * UiUploader
 *
 */

import React, { memo, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import Uploader from 'rc-upload';
import { Button, CircularProgress } from '@material-ui/core';
import ErrorIcon from '@material-ui/icons/ErrorOutlineOutlined';
import { fileUpload } from '../../constants/api';
import { non } from '../../utils/function';

export const useUploader = (defaltValue = []) => {
  const [attachments, setAttachments] = useState(defaltValue);
  const onAddAttachment = useCallback(
    result => {
      setAttachments([...attachments, result.data]);
    },
    [attachments],
  );
  return [attachments, onAddAttachment];
};

function UploadStatus({ error, isLoading }) {
  if (error) {
    return (
      <span title={error}>
        <ErrorIcon color="error" />
      </span>
    );
  }

  if (isLoading) {
    return <CircularProgress />;
  }
  return null;
}
UploadStatus.propTypes = {
  error: PropTypes.string,
  isLoading: PropTypes.bool,
};

function UiUploader({
  accept = '',
  data = {},
  authorization,
  onSuccess = non,
  onStart = non,
  onError = non,
  route = fileUpload,
  title = 'upload',
  disabled = false,
  render,
}) {
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState('');
  return (
    <Uploader
      {...{
        accept,

        action: route,
        headers: {
          Authorization: authorization,
        },
        beforeUpload() {
          setError('');
          // console.log('beforeUpload', file.name);
        },
        onStart: file => {
          // console.log('onStart', file.name);
          setLoading(true);
          onStart(file);
          // this.refs.inner.abort(file);
        },
        onSuccess(file) {
          // console.log('onSuccess', file);
          setLoading(false);
          onSuccess(file);
        },
        // onProgress(step, file) {
        // console.log('onProgress', Math.round(step.percent), file.name);
        // },
        onError(err) {
          // console.log('onError', err.message);
          setLoading(false);
          onError(err.message);
          setError(err.message);
          // setLoading(false)
        },
      }}
      data={data}
      disabled={isLoading || disabled}
    >
      {render || (
        <React.Fragment>
          <Button
            variant="contained"
            color="primary"
            disabled={isLoading || disabled}
          >
            {title}
          </Button>
          <UploadStatus isLoading={isLoading} error={error} />
        </React.Fragment>
      )}
    </Uploader>
  );
}

UiUploader.propTypes = {
  accept: PropTypes.string,
  authorization: PropTypes.string.isRequired,
  data: PropTypes.object,
  onError: PropTypes.func,
  onStart: PropTypes.func,
  onSuccess: PropTypes.func,
  route: PropTypes.string,
  title: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  render: PropTypes.node,
};

export default memo(UiUploader);
