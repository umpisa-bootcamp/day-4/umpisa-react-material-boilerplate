/**
 *
 * TitleWithButtons
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Typography, Button, Grid } from '@material-ui/core';
import { useStyles } from './style';

function TitleWithButtons({ title, actions = [], renderTitle, titleSuffix }) {
  const classes = useStyles();

  return (
    <Grid container className={classes.root}>
      <Grid item xs={12} sm={12} md={6}>
        {renderTitle ? (
          <span className={classes.title}>{renderTitle}</span>
        ) : (
          <Typography variant="h4" className={classes.title}>
            {title}
            {titleSuffix}
          </Typography>
        )}
      </Grid>
      <Grid
        item
        xs={12}
        sm={12}
        md={6}
        container
        justify="flex-end"
        className={classes.buttonGrid}
      >
        <div className={classes.buttonGroup}>
          {actions.map(item => {
            if (item.isHidden) {
              return null;
            }

            if (item.render) {
              return item.render;
            }
            return (
              <Button
                key={item.title}
                variant="contained"
                color="primary"
                {...item}
              >
                {item.title}
              </Button>
            );
          })}
        </div>
      </Grid>
    </Grid>
  );
}

TitleWithButtons.propTypes = {
  title: PropTypes.string.isRequired,
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      isHidden: PropTypes.bool,
      title: PropTypes.string.isRequired,
      onClick: PropTypes.func,
    }),
  ),
  titleSuffix: PropTypes.node,
  renderTitle: PropTypes.node,
};

export default memo(TitleWithButtons);
