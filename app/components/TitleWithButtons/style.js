import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
  },
  title: {
    marginRight: 'auto',
    fontWeight: 'bold',
  },
  buttonGroup: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  buttonGrid: {
    [theme.breakpoints.only('xs')]: {
      marginTop: 20,
      justifyContent: 'center',
    },
  },
}));
