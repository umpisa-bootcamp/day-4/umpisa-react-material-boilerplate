/**
 *
 * UiLogout
 *
 */

import React, { memo, useState, useCallback } from 'react';
import {
  ListItemIcon,
  ListItemText,
  ListItem,
  Tooltip,
  Dialog,
  DialogActions,
  DialogContentText,
  DialogContent,
  DialogTitle,
  Button,
} from '@material-ui/core';
// import PropTypes from 'prop-types';
import history from 'utils/history';
import { logoutIcon } from '../../constants/images';
import { useStyles } from './style';

import { authLogout } from '../../constants/route';

function UiLogout() {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const onNext = useCallback(() => {
    setOpen(false);
    history.push(authLogout);
  }, []);

  const handleClickOpen = useCallback(() => {
    setOpen(true);
  }, [setOpen]);

  const handleClose = useCallback(() => {
    setOpen(false);
  }, [setOpen]);

  return (
    <React.Fragment>
      <Tooltip title="Log out" placement="right-start">
        <ListItem button onClick={handleClickOpen}>
          <ListItemIcon>
            <img src={logoutIcon} alt="logout" className={classes.turnOff} />
          </ListItemIcon>
          <ListItemText primary="Log out" />
        </ListItem>
      </Tooltip>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Confirmation</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure you want to logout?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={onNext} color="primary" autoFocus>
            Logout
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}

UiLogout.propTypes = {};

export default memo(UiLogout);
