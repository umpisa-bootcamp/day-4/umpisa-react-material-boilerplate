import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  turnOff: {
    width: '21px',
    height: '21px',
    transform: 'rotate(90deg)',
  },
}));
