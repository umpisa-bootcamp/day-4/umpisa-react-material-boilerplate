import React from 'react';
import PropTypes from 'prop-types';
import {
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
} from '@material-ui/core';
import Search from '@material-ui/icons/Search';

const SearchBar = ({ onChange, value }) => (
  <FormControl variant="outlined" margin="dense">
    <InputLabel>Search</InputLabel>
    <OutlinedInput
      onChange={onChange}
      value={value}
      endAdornment={
        <InputAdornment position="end">
          <Search />
        </InputAdornment>
      }
      labelWidth={70}
    />
  </FormControl>
);

SearchBar.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default SearchBar;
