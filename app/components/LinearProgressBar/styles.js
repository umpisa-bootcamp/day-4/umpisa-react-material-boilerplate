import { makeStyles, withStyles, LinearProgress } from '@material-ui/core';

export const CustomProgressBar = withStyles({
  root: {
    height: 10,
    borderRadius: 20,
    backgroundColor: 'rgba(36, 78, 96, 0.08);',
    boxShadow: 'inset 0px 2px 4px rgba(0, 0, 0, 0.08);',
  },
  bar: {
    borderRadius: 20,
  },
  barColorPrimary: {
    backgroundColor: '#27B470',
  },
  barColorSecondary: {
    backgroundColor: '#EB2D4F',
  },
})(LinearProgress);

export const useStyles = makeStyles(() => ({
  margin: {
    padding: 12,
    zIndex: 1,
  },
  progressLabel: {
    margin: '10px 0 10px 0',
    top: 3,
    bottom: 0,
    color: '#828282',
    fontSize: '14px !important',
  },
}));
