/**
 *
 * LinearProgressBar
 *
 */

import React, { memo } from 'react';
import { Box, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import { CustomProgressBar, useStyles } from './styles';

function LinearProgressBar({ textValue, percentage, ...props }) {
  const classes = useStyles();

  return (
    <Box component="div" position="relative">
      <CustomProgressBar className={classes.margin} {...props} />
      <Box component="div" display="flex" justifyContent="space-between">
        <Typography className={classes.progressLabel}>{textValue}</Typography>
        <Typography className={classes.progressLabel}>
          {percentage} %
        </Typography>
      </Box>
    </Box>
  );
}

LinearProgressBar.propTypes = {
  textValue: PropTypes.string,
  percentage: PropTypes.string,
};

export default memo(LinearProgressBar);
