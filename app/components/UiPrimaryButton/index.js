/**
 *
 * UiPrimaryButton
 *
 */

import { memo } from 'react';
// import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { teal } from '@material-ui/core/colors';
import { themeColor } from '../../constants/color';

const UiPrimaryButton = withStyles(theme => ({
  root: {
    color: theme.palette.getContrastText(teal[500]),
    backgroundColor: themeColor,
    '&:hover': {
      backgroundColor: teal[800],
    },
  },
}))(Button);

// UiPrimaryButton.propTypes = {};

export default memo(UiPrimaryButton);
