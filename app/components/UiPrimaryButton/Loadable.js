/**
 *
 * Asynchronously loads the component for UiPrimaryButton
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
