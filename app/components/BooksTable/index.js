/**
 *
 * BooksTable
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Table, TableRow, TableHead, TableBody } from '@material-ui/core';
import BooksTableRow from '../BooksTableRow';

function BooksTable({ books, onClickRow }) {
  return (
    <Table>
      <TableHead>
        <TableRow>
          <th>Title</th>
          <th>Description</th>
        </TableRow>
      </TableHead>
      <TableBody>
        {books.map((book, index) => (
          <BooksTableRow
            key={`books_tr_${book.title}`}
            book={book}
            index={index}
            onClickRow={onClickRow}
          />
        ))}
      </TableBody>
    </Table>
  );
}

BooksTable.propTypes = {
  books: PropTypes.array.isRequired,
  onClickRow: PropTypes.func.isRequired,
};

export default BooksTable;
