/**
 *
 * Footer
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';

function Footer({ children, style, ...props }) {
  return (
    <footer
      style={{
        position: 'absolute',
        right: 0,
        bottom: 0,
        left: 0,
        textAlign: 'center',
        ...style,
      }}
      {...props}
    >
      {children}
    </footer>
  );
}

Footer.propTypes = {
  children: PropTypes.node,
  style: PropTypes.object,
};

export default memo(Footer);
