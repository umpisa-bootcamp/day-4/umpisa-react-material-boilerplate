/**
 *
 * BooksFormInput
 *
 */

import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';

import { TextField, Button, Grid } from '@material-ui/core';

function BooksFormInput({ onSubmit }) {
  const [state, setState] = useState({
    title: '',
    description: '',
  });

  const onChangeField = (event, field) => {
    setState({
      ...state,
      [field]: event.target.value,
    });
  };

  return (
    <Grid container direction="column">
      <Grid>
        <TextField
          label="title"
          onChange={event => onChangeField(event, 'title')}
        />
      </Grid>
      <Grid>
        <TextField
          label="description"
          onChange={event => onChangeField(event, 'description')}
        />
      </Grid>
      <Grid>
        <Button onClick={() => onSubmit(state)}>Save</Button>
      </Grid>
    </Grid>
  );
}

BooksFormInput.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default memo(BooksFormInput);
