import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  appLogo: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'row',
  },
  logo: {
    alignSelf: 'center',
    width: '370px',
    height: '155px',
    [theme.breakpoints.only('xs')]: {
      width: '75%',
      height: '75%',
    },
  },
  header: {
    width: '152.77px',
    height: '64px',
  },
  avatar: {
    margin: 10,
  },
  displayCenter: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));
