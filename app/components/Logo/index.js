/**
 *
 * Logo
 *
 */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import { useStyles } from './style';

function Logo({ position, auth, history }) {
  const classes = useStyles();
  return (
    <div
      className={position ? classes.displayCenter : classes.appLogo}
      onClick={() => {
        history.push(`/dashboard`);
      }}
    >
      <Avatar className={classes.avatar}>J</Avatar>
    </div>
  );
}

Logo.propTypes = {
  position: PropTypes.bool,
  auth: PropTypes.bool,
  history: PropTypes.object,
};

export default memo(Logo);
