/**
 *
 * ConditionalDisplay
 *
 */

import { memo } from 'react';
import PropTypes from 'prop-types';

function ConditionalDisplay({ children, visible = true }) {
  if (visible) {
    return children;
  }
  return null;
}

ConditionalDisplay.propTypes = {
  children: PropTypes.node,
  visible: PropTypes.bool,
};

export default memo(ConditionalDisplay);
