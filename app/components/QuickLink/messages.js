/*
 * QuickLink Messages
 *
 * This contains all the text for the QuickLink component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.QuickLink';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the QuickLink component!',
  },
});
