/**
 *
 * QuickLink
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

function QuickLink() {
  return (
    <div>
      <FormattedMessage {...messages.header} />
    </div>
  );
}

QuickLink.propTypes = {};

export default memo(QuickLink);
