/**
 *
 * Asynchronously loads the component for QuickLink
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
