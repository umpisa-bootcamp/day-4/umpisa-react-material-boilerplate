/**
 *
 * Asynchronously loads the component for UiTable
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
