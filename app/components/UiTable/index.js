/**
 *
 * UiTable
 *
 */

import React, { memo, useState, useCallback } from 'react';
import PropTypes from 'prop-types';

import { Checkbox, Typography, Button } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ArrowDown from '@material-ui/icons/KeyboardArrowDown';

import { StyledTableRow, useStyles } from './style';
export const tableSize = {
  small: 'small',
  medium: 'medium',
};

export const tableDefault = {
  size: tableSize.small,
  selectable: false,
  uniqueKey: 'id',
  delete: true,
};
export const tableHeaderCellDefaultProps = {
  align: 'left',
  style: {
    fontWeight: 'bold',
    fontSize: 16,
  },
};

export const tableCellDefaultProps = {
  align: 'left',
};

const generateCell = ({ col = {}, row, classes }) => {
  const { cellProps = {} } = col;
  return (
    <TableCell
      className={classes.tableCell}
      key={col.key}
      {...{ ...tableCellDefaultProps, ...cellProps }}
      onClick={col.onClick ? () => col.onClick(row) : undefined}
    >
      {col.render ? col.render(row) : row[col.field]}
    </TableCell>
  );
};
generateCell.propTypes = {
  col: PropTypes.object,
  row: PropTypes.object,
  classes: PropTypes.object,
};
const generateHeaderCell = ({ col, classes, order = {} }) => {
  const { headerProps = {}, onFilter = () => {} } = col;
  const { style = {} } = headerProps;
  const hasFilter = Boolean(col.filter);
  const allStyles = { ...tableHeaderCellDefaultProps.style, ...style };
  return (
    <TableCell
      className={classes.tableCell}
      key={col.key}
      {...{ ...tableHeaderCellDefaultProps, ...headerProps, style: allStyles }}
    >
      <Button disabled={!hasFilter} onClick={onFilter}>
        <Typography variant="h5" className={classes.tableHeadTitle}>
          {col.name}{' '}
          {col.name && hasFilter && col.field in order ? <ArrowDown /> : null}
        </Typography>
      </Button>
    </TableCell>
  );
};
generateHeaderCell.propTypes = {
  col: PropTypes.object,
  classes: PropTypes.object,
  order: PropTypes.object,
};

const generateSelectableCell = ({ classes, onSelect, isSelected, data }) => (
  <TableCell className={classes.checkboxCell} {...tableCellDefaultProps}>
    <Checkbox
      className={classes.checkbox}
      color="primary"
      size="small"
      checked={isSelected}
      onChange={(e, v) => onSelect(data, e, v)}
      value={data}
    />
  </TableCell>
);
generateSelectableCell.propTypes = {
  classes: PropTypes.object,
  onSelect: PropTypes.func,
  isSelected: PropTypes.bool,
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

const generateSelectableHeaderCell = ({ classes, onSelectAll }) => (
  <TableCell className={classes.checkboxCell} {...tableHeaderCellDefaultProps}>
    <Checkbox
      onChange={onSelectAll}
      className={classes.checkbox}
      color="primary"
      size="small"
    />
  </TableCell>
);
generateSelectableHeaderCell.propTypes = {
  classes: PropTypes.object,
  onSelectAll: PropTypes.func,
};

// const hasSelected = (selected = {}) => {
//   const v = Object.keys(selected).find(k => selected[k]);
//   return !!v;
// };

function UiTable({
  columns,
  tabs,
  items = [],
  config = {},
  isLoading = false,
  order = {},
}) {
  const classes = useStyles();
  const mergeConfig = { ...tableDefault, ...config };

  const [selected, setSelected] = useState(
    items.reduce((p, c) => ({ ...p, [c[mergeConfig.uniqueKey]]: false }), {}),
  );

  const onSelect = useCallback(
    (rowData, e, v) => {
      const key = rowData[mergeConfig.uniqueKey];
      setSelected({ ...selected, [key]: v, event: e });
    },
    [selected, onSelect, items],
  );

  const onSelectAll = useCallback(
    (e, v) => {
      const all = items.reduce(
        (p, c) => ({ ...p, [c[mergeConfig.uniqueKey]]: v, event: e }),
        {},
      );
      setSelected(all);
    },
    [selected, onSelect, items],
  );

  return (
    <Paper className={classes.paper}>
      <div className={classes.wrapper}>
        {tabs}
        <div className={classes.content}>
          <Table className={classes.table} size={mergeConfig.size}>
            <TableHead className={classes.tableHead}>
              <TableRow>
                {mergeConfig.selectable
                  ? generateSelectableHeaderCell({ classes, onSelectAll })
                  : null}
                {columns.map(col =>
                  generateHeaderCell({ classes, col, config, order }),
                )}
              </TableRow>
            </TableHead>
            <TableBody>
              {isLoading
                ? null
                : items.map(row => (
                  <StyledTableRow className={classes.tableRow} key={row._id}>
                    {mergeConfig.selectable
                      ? generateSelectableCell({
                        classes,
                        onSelect,
                        data: row,
                        isSelected: Boolean(
                          selected[row[mergeConfig.uniqueKey]],
                        ),
                      })
                      : null}
                    {columns.map(col => generateCell({ classes, col, row }))}
                  </StyledTableRow>
                ))}
            </TableBody>
          </Table>
        </div>
      </div>
    </Paper>
  );
}

UiTable.propTypes = {
  columns: PropTypes.array.isRequired,
  items: PropTypes.array.isRequired,
  config: PropTypes.object,
  order: PropTypes.object,
  isLoading: PropTypes.bool,
  tabs: PropTypes.object,
};

export default memo(UiTable);
