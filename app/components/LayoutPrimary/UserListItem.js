import React, { memo } from 'react';
import PropTypes from 'prop-types';

import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import CloudUploadOutlinedIcon from '@material-ui/icons/CloudUploadOutlined';
import RouteListItem from './RouteListItem';

import * as routes from '../../constants/route';
import UiLogout from '../UiLogout';

const UserListItem = ({
  selectedMenuItem,
  setSelectedMenuItem,
  isMenuOpen,
}) => {
  return (
    <div>
      <RouteListItem
        to={routes.tenantAccount}
        title="Dashboard"
        selectedMenuItem={selectedMenuItem}
        setSelectedMenuItem={setSelectedMenuItem}
      >
        <ListItemIcon>
          <CloudUploadOutlinedIcon />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </RouteListItem>
      <UiLogout />
    </div>
  );
};
UserListItem.propTypes = {
  selectedMenuItem: PropTypes.string.isRequired,
  setSelectedMenuItem: PropTypes.func.isRequired,
  isMenuOpen: PropTypes.bool.isRequired,
};

export default memo(UserListItem);
