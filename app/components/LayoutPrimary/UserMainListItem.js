import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { ListItemIcon, ListItemText } from '@material-ui/core';
import {
  CloudUploadOutlined,
  AddIcon,
} from '@material-ui/icons';
import * as routes from '../../constants/route';
import RouteListItem from './RouteListItem';
import UiLogout from '../UiLogout';

const UserMainListItems = ({ selectedMenuItem, setSelectedMenuItem }) => (
  <div>
    <RouteListItem
      to={routes.dashboard}
      title="Dashboard"
      selectedMenuItem={selectedMenuItem}
      setSelectedMenuItem={setSelectedMenuItem}
    >
      <ListItemIcon>
        <CloudUploadOutlined />
      </ListItemIcon>
      <ListItemText primary="Dashboard" />
    </RouteListItem>

    {/* <RouteListItem
      to={routes.booksCreate}
      title="Create Book"
      selectedMenuItem={selectedMenuItem}
      setSelectedMenuItem={setSelectedMenuItem}
    >
      <ListItemIcon>
        <AddIcon />
      </ListItemIcon>
      <ListItemText primary="Create Book" />
    </RouteListItem> */}

    <UiLogout />
  </div>
);
UserMainListItems.propTypes = {
  selectedMenuItem: PropTypes.string.isRequired,
  setSelectedMenuItem: PropTypes.func.isRequired,
};

export default memo(UserMainListItems);
