import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { ListItem, Tooltip } from '@material-ui/core';

// fix problem in ref of native element to material ui
const CollisionLink = React.forwardRef((props, ref) => (
  <NavLink innerRef={ref} {...props} />
));

export const RouteListItem = ({
  children,
  title = '',
  to,
  selectedMenuItem,
  setSelectedMenuItem = () => {},
  selected = false,
  ...props
}) => (
  <Tooltip title={title} placement="right-start">
    <ListItem
      button
      component={CollisionLink}
      to={to}
      onClick={() => setSelectedMenuItem(to)}
      selected={selected || selectedMenuItem === to}
      {...props}
    >
      {children}
    </ListItem>
  </Tooltip>
);

RouteListItem.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string.isRequired,
  selectedMenuItem: PropTypes.string.isRequired,
  setSelectedMenuItem: PropTypes.func.isRequired,
  selected: PropTypes.bool,
  to: PropTypes.string.isRequired,
};

export default memo(RouteListItem);
