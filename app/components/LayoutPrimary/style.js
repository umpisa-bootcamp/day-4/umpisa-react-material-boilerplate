import { makeStyles } from '@material-ui/core/styles';
import { themeColor, white, grey1, blue1 } from '../../constants/color';

const drawerWidth = 250;

export const listItemIcon = {
  minWidth: 0,
  padding: '0 15px 0 0',
};

export const turnOff = {
  width: '21px',
  height: '21px',
  transform: 'rotate(90deg)',
};
export const useStyles = makeStyles(theme => ({
  profileSection: {
    padding: '18px 20px',
  },
  myName: {
    color: grey1,
    fontFamily: 'Roboto',
    fontSize: '16px',
    lineHeight: '20px',
    marginBottom: '15px',
  },
  renew: {
    fontWeight: 'bold',
    color: blue1,
  },
  menuItem: {
    position: 'absolute',
    top: '48px',
    right: '0',
    height: '1px',
    width: '100%',
    background: '#1F4252',
  },
  root: {
    display: 'flex',
  },
  logoutIcon: {
    width: '21px',
    height: '21px',
  },

  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    backgroundColor: themeColor,
    ...theme.mixins.toolbar,
  },
  appBar: {
    backgroundColor: themeColor,
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: '100%',
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  bgWhite: {
    color: 'rgba(0, 0, 0, 0.87)',
    backgroundColor: white,
  },
  bgTheme: {
    color: white,
    backgroundColor: themeColor,
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    paddingTop: theme.spacing(25),
    paddingBottom: theme.spacing(4),
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    marginLeft: 0,
  },
  container: {},
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  drawer: {
    width: 60,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    display: 'flex',
    justifyContent: 'space-between',
    paddingTop: '80px',
    backgroundColor: themeColor,
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    display: 'flex',
    justifyContent: 'space-between',
    paddingTop: '80px',
    backgroundColor: themeColor,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    // width: theme.spacing(10) + 1,
    width: 60,
    // [theme.breakpoints.up('sm')]: {
    //   width: theme.spacing(9) + 1,
    // },
  },
  userName: {
    margin: '0 20px 0 20px',
    color: '#fff',
    fontSize: '20px',
    textTransform: 'capitalize',
  },
}));
