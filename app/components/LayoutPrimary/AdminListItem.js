import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { ListItemIcon, ListItemText } from '@material-ui/core';
import {
  CloudUploadOutlined,
} from '@material-ui/icons';

import * as routes from '../../constants/route';
import RouteListItem from './RouteListItem';
import UiLogout from '../UiLogout';


const AdminListItems = ({
  selectedMenuItem,
  setSelectedMenuItem,
  isAdmin,
  isMenuOpen,
}) => {
  return (
    <div>
      <RouteListItem
        to={routes.dashboard}
        title="Dashboard"
        selectedMenuItem={selectedMenuItem}
        setSelectedMenuItem={setSelectedMenuItem}
      >
        <ListItemIcon>
          <CloudUploadOutlined />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </RouteListItem>
      <UiLogout />
    </div>
  );
};

AdminListItems.propTypes = {
  isAdmin: PropTypes.bool,
  selectedMenuItem: PropTypes.string.isRequired,
  setSelectedMenuItem: PropTypes.func.isRequired,
  isMenuOpen: PropTypes.bool.isRequired,
};
export default memo(AdminListItems);
