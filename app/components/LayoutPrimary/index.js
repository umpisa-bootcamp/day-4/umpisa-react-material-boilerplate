/**
 *
 * LayoutPrimary
 *
 */

import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import List from '@material-ui/core/List';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import LinearProgress from '@material-ui/core/LinearProgress';
import Button from '@material-ui/core/Button';

import { non } from 'utils/function';
import AdminListItems from './AdminListItem';
import UserMainListItems from './UserMainListItem';
import Logo from '../Logo';

import { useStyles } from './style';
import unallowedRoutes from './constant';

import composer from './composer';
import ThemeDark from '../ThemeDark';
import { getRecords } from '../../utils/helper/modelHelper';

function LayoutPrimary({
  children,
  isAdmin,
  isLoading,
  required = true,
  location: { pathname } = {},
  auth,
  history,
}) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [selectedMenuItem, setSelectedMenuItem] = React.useState('');

  const isInitialMount = useRef(true);
  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
      setSelectedMenuItem(history.location.pathname);
    }
  });

  const handleDrawerOpen = () => {
    setOpen(!open);
  };

  return unallowedRoutes.includes(pathname) ? (
    <div>{children}</div>
  ) : (
    <div className={classes.root}>
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar className={classes.toolbar}>
          <Button onMouseEnter={handleDrawerOpen}>
            <Logo history={history} />
            {auth ? (
              <div className={classes.userName}>Hi, {auth.name}!</div>
            ) : null}
          </Button>
        </Toolbar>
      </AppBar>
      <ThemeDark>
        <Drawer
          variant="permanent"
          className={classes.drawer}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
          open={open}
          onMouseLeave={() => setOpen(false)}
          onMouseEnter={() => setOpen(true)}
        >
          <List>
            {auth && auth.isAdmin ? (
              <AdminListItems
                isAdmin={isAdmin}
                selectedMenuItem={selectedMenuItem}
                setSelectedMenuItem={setSelectedMenuItem}
                setOpenMenu={setOpen}
                isMenuOpen={open}
              />
            ) : (
              <UserMainListItems
                selectedMenuItem={selectedMenuItem}
                setSelectedMenuItem={setSelectedMenuItem}
                setOpenMenu={setOpen}
                isMenuOpen={open}
              />
            )}
          </List>
        </Drawer>
      </ThemeDark>
      <main className={classes.content}>
        {getContent(isLoading, required, children)}
      </main>
    </div>
  );
}

const getContent = (isLoading, required, children) => {
  if (isLoading) {
    return <LinearProgress />;
  }

  if (!required) {
    return '';
  }

  return children;
};

LayoutPrimary.propTypes = {
  required: PropTypes.any,
  children: PropTypes.node,
  isLoading: PropTypes.bool,
  location: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  isAdmin: PropTypes.bool,
  history: PropTypes.object.isRequired,
};

export default composer(LayoutPrimary);
