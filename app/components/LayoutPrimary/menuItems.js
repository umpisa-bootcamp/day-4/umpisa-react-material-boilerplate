// DEPRECATED
import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PowerSettingsNew from '@material-ui/icons/PowerSettingsNew';

import { Tooltip } from '@material-ui/core';
import * as routes from '../../constants/route';
import { turnOff } from './style';
const CollisionLink = React.forwardRef((props, ref) => (
  <NavLink innerRef={ref} {...props} />
));

const RouteListItem = ({
  children,
  title = '',
  selectedMenuItem,
  setSelectedMenuItem,
  ...props
}) => (
  <Tooltip title={title} placement="right-start">
    <ListItem
      button
      component={CollisionLink}
      {...props}
      onClick={() => setSelectedMenuItem(title)}
      selected={selectedMenuItem === title}
    >
      {children}
    </ListItem>
  </Tooltip>
);

RouteListItem.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string.isRequired,
  selectedMenuItem: PropTypes.string.isRequired,
  setSelectedMenuItem: PropTypes.func.isRequired,
};

export const mainListItems = ({
  selectedMenuItem,
  setSelectedMenuItem,
  isAdmin,
}) => {
  return (
    <div>
      <RouteListItem
        to={routes.dashboard}
        title="Dashboard"
        selectedMenuItem={selectedMenuItem}
        setSelectedMenuItem={setSelectedMenuItem}
      >
        <ListItemIcon>
          <svg
            width="24"
            height="24"
            viewBox="0 0 32 32"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect
              x="18"
              y="1"
              width="13"
              height="30"
              rx="2"
              stroke="white"
              strokeWidth="2"
            />
            <rect
              x="1"
              y="1"
              width="13"
              height="16"
              rx="2"
              stroke="white"
              strokeWidth="2"
            />
            <rect
              x="1"
              y="21"
              width="13"
              height="10"
              rx="2"
              stroke="white"
              strokeWidth="2"
            />
          </svg>
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </RouteListItem>
      <RouteListItem to={routes.authLogout} title="Log out">
        <ListItemIcon>
          <img src={logoutIcon} alt="logout" style={turnOff} />
        </ListItemIcon>
        <ListItemText primary="Log out" />
      </RouteListItem>
    </div>
  );
};
mainListItems.propTypes = {
  isAdmin: PropTypes.bool,
  selectedMenuItem: PropTypes.string.isRequired,
  setSelectedMenuItem: PropTypes.func.isRequired,
};

export const userMainListItems = ({
  selectedMenuItem,
  setSelectedMenuItem,
}) => (
  <div>
    <RouteListItem
      to={routes.tenantAccount}
      title="Dashboard"
      selectedMenuItem={selectedMenuItem}
      setSelectedMenuItem={setSelectedMenuItem}
    >
      <ListItemIcon>
        <img src={dashboardIcon} alt="dashboard" width={24} height={24} />
      </ListItemIcon>
      <ListItemText primary="Dashboard" />
    </RouteListItem>
    <RouteListItem to={routes.authLogout} title="Log out">
      <ListItemIcon>
        <img
          src={logoutIcon}
          alt="logout"
          style={turnOff}
          width={24}
          height={24}
        />
      </ListItemIcon>
      <ListItemText primary="Log out" />
    </RouteListItem>
  </div>
);
userMainListItems.propTypes = {
  selectedMenuItem: PropTypes.string.isRequired,
  setSelectedMenuItem: PropTypes.func.isRequired,
};

export const logout = (
  <RouteListItem to={routes.authLogout} title="Logout">
    <ListItemText primary="Logout" />
    <ListItemIcon>
      <PowerSettingsNew />
    </ListItemIcon>
  </RouteListItem>
);
