import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { authSignin } from 'constants/route';

const RedirectToLogin = props => (
  <Redirect
    to={{
      pathname: authSignin,
      state: { from: props.location },
    }}
  />
);
RedirectToLogin.propTypes = {
  location: PropTypes.object,
};

export default RedirectToLogin;
