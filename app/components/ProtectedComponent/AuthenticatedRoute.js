import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import Redirect from './RedirectToLogin';
import RedirecToHome from './RedirectToHome';

const AuthenticatedRoute = ({
  component: Component,
  isLogged = false,
  role,
  isAdmin = false,
  adminOnly = false,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      if (!isLogged) {
        return <Redirect {...props} />;
      }
      if (adminOnly && !isAdmin) {
        return <RedirecToHome />;
      }

      return <Component {...props} {...rest} />;
    }}
  />
);

AuthenticatedRoute.propTypes = {
  component: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.element,
    PropTypes.func,
  ]).isRequired,
  isAuthenticated: PropTypes.bool,
  isLogged: PropTypes.bool,
  allowedRoles: PropTypes.array,
  role: PropTypes.string,
  isAdmin: PropTypes.bool,
  adminOnly: PropTypes.bool,
};

export default AuthenticatedRoute;
