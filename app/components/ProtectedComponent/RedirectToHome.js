import React from 'react';
import { Redirect } from 'react-router-dom';

const RedirecToHome = () => (
  <Redirect
    to={{
      pathname: '/dashboard',
    }}
  />
);

export default RedirecToHome;
