import { makeStyles, withStyles, CircularProgress } from '@material-ui/core';
import { mediumGreen } from '../../constants/color';

export const CustomCircularProgressBar = withStyles({
  colorPrimary: {
    color: mediumGreen,
  },
})(CircularProgress);

export const useStyles = makeStyles(() => ({
  percentage: {
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: 64,
    height: 64,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));
