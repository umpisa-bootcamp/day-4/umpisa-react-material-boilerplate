/**
 *
 * Asynchronously loads the component for CircularProgressBar
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
