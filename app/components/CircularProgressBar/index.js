/**
 *
 * CircularProgressBar
 *
 */

import React, { memo } from 'react';
import { Box } from '@material-ui/core';
import PropTypes from 'prop-types';
import { CustomCircularProgressBar, useStyles } from './style';

function CircularProgressBar({ percentage, ...props }) {
  const classes = useStyles();
  return (
    <Box component="div" position="relative">
      <Box component="div">
        <CustomCircularProgressBar {...props} />
        <Box component="div" position="absolute" className={classes.percentage}>
          {percentage}%
        </Box>
      </Box>
    </Box>
  );
}

CircularProgressBar.propTypes = {
  percentage: PropTypes.any,
};

export default memo(CircularProgressBar);
