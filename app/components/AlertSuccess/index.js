/**
 *
 * AlertSuccess
 *
 */

import React, { memo, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  Dialog,
  Typography,
  Grid,
} from '@material-ui/core';
import { CheckCircle, CancelRounded } from '@material-ui/icons';
import UiSpacer from '../UiSpacer';

import { useStyles } from './style';

// hooks
export const useAlertState = () => {
  const [alertSuccess, evAlertSuccess] = useState(false);
  const onCloseAlertSucces = useCallback(() => evAlertSuccess(false));
  const onOpenAlertSuccess = useCallback(() => evAlertSuccess(true));
  return [alertSuccess, onOpenAlertSuccess, onCloseAlertSucces];
};

function AlertSuccess({ message, title, isOpen, onClose, success = true }) {
  const classes = useStyles();
  return (
    <Dialog
      style={{ zIndex: 99999 }}
      open={isOpen}
      onClose={onClose}
      maxWidth="sm"
      fullWidth
    >
      <DialogTitle className={classes.green}>{title}</DialogTitle>
      <DialogContent>
        <Grid container direction="row" justify="space-between">
          <div style={{ width: 410 }}>
            <UiSpacer />
            <Typography>{message}</Typography>
          </div>
          {success ? (
            <CheckCircle style={{ fontSize: 105 }} className={classes.green} />
          ) : (
            <CancelRounded style={{ fontSize: 105 }} className={classes.red} />
          )}
        </Grid>
      </DialogContent>
      <DialogActions className={classes.buttonGroup}>
        <Button onClick={onClose} color="inherit">
          dismiss
        </Button>
      </DialogActions>
    </Dialog>
  );
}

AlertSuccess.propTypes = {
  message: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  success: PropTypes.bool,
};

export default memo(AlertSuccess);
