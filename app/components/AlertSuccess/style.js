import { makeStyles } from '@material-ui/core/styles';
export const useStyles = makeStyles(theme => ({
  buttonGroup: {
    paddingRight: theme.spacing(7),
  },
  green: {
    color: '#4BB543',
  },
  red: {
    color: '#EB5757',
  },
}));
