/**
 *
 * Asynchronously loads the component for TextFieldSelect
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
