/**
 *
 * TextFieldSelect
 *
 */

import React, { memo, useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { useDebouncedCallback } from 'use-debounce';
import { isEmpty } from 'lodash';
import { non } from 'utils/function';

const overrideStyles = {
  menuPortal: base => ({ ...base, zIndex: 9999 }),
  menu: provided => ({ ...provided, zIndex: '9999 !important' }),
};

function TextFieldSelect({
  onSearch,
  getOptionValue,
  onSelect,
  getOptionLabel = non,
  valueReducer = non,
  options = [],
  defaultValue = '',
  ...props
}) {
  const [handleSearch] = useDebouncedCallback(inputValue => {
    onSearch(inputValue);
  }, 500);

  const handleInputChange = (inputValue, { action }) => {
    if (action === 'input-change') handleSearch(inputValue);
  };

  const handleSelect = useCallback(
    obj => {
      onSelect(getOptionValue(obj));
    },
    [onSelect, getOptionValue],
  );

  const handleOptionLabel = useCallback(
    unit => {
      if (isEmpty(unit)) return '';
      return getOptionLabel(unit);
    },
    [getOptionLabel],
  );

  const value = useMemo(() => options.find(valueReducer) || defaultValue, [
    valueReducer,
    options,
  ]);

  return (
    <Select
      styles={overrideStyles}
      isSearchable
      getOptionValue={getOptionValue}
      onInputChange={handleInputChange}
      onChange={handleSelect}
      getOptionLabel={handleOptionLabel}
      menuPortalTarget={document.body}
      options={options}
      {...(valueReducer === non ? {} : { value })}
      {...props}
    />
  );
}

TextFieldSelect.propTypes = {
  onChange: PropTypes.func,
  getOptionValue: PropTypes.func,
  onSelect: PropTypes.func,
  onSearch: PropTypes.func,
  getOptionLabel: PropTypes.func,
  valueReducer: PropTypes.func,
  options: PropTypes.array,
  defaultValue: PropTypes.any,
};

export default memo(TextFieldSelect);
