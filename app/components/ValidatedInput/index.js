/**
 *
 * ValidatedInput
 *
 */

import React, { memo, useRef, useState } from 'react';
import { TextField } from '@material-ui/core';
import PropTypes from 'prop-types';

// [{
//   condition: true,
//   message: ''
// }]

function ValidatedInput({ onChange, onInputError, onBlur, ...props }) {
  const initialState = { hasError: false, errorMessage: '' };
  const [state, setState] = useState(initialState);
  const inputRef = useRef();

  const handleError = (errors = []) => {
    if (errors) {
      if (!errors.hasOwnProperty('length')) {
        throw new Error(`Expected array but got ${typeof errors}`);
      } else if (
        !errors.every(
          error =>
            Object.keys(error).includes('condition') &&
            Object.keys(error).includes('message'),
        )
      ) {
        throw new Error('condition and message property');
      } else {
        const filteredErrors = errors.filter(error => error.condition);

        if (filteredErrors.length > 0) {
          const errorMessage = filteredErrors[0] && filteredErrors[0].message;
          setState(oldState => ({
            ...oldState,
            hasError: filteredErrors.length > 0,
            errorMessage,
          }));
        } else {
          setState(initialState);
        }
      }
    } else {
      setState(initialState);
    }
  };

  const handleChange = e => {
    onInputError && handleError(onInputError(e.target.value));
    onChange && onChange(e);
  };

  const handleBlur = e => {
    const input = inputRef.current.querySelector('input');
    if (props.required) {
      if (!input.value) {
        setState(oldState => ({
          ...oldState,
          hasError: true,
          errorMessage: 'This field is required',
        }));
      } else if (onInputError) {
        handleError(onInputError(e.target.value));
      } else {
        setState(initialState);
      }
      onBlur && onBlur(e);
    }
  };

  return (
    <TextField
      error={state.hasError}
      onBlur={handleBlur}
      helperText={state.errorMessage}
      ref={inputRef}
      onChange={handleChange}
      {...props}
    />
  );
}

ValidatedInput.propTypes = {
  onChange: PropTypes.func,
  onInputError: PropTypes.func,
  onBlur: PropTypes.func,
  required: PropTypes.bool,
};

export default memo(ValidatedInput);
