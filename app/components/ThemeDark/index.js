/**
 *
 * ThemeDark
 *
 */

import React, { memo } from 'react';
import { ThemeProvider } from '@material-ui/styles';
import PropTypes from 'prop-types';
import { createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
  spacing: 4,
  typography: {
    fontFamily: [
      // may bayad tong avenir
      // kapag wala pa sa system nya. san serif nlang tutal magmukha den lang
      'Avenir',
      'sans-serif',
      'Roboto',
      'Raleway',
      'Arial',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      '"Helvetica Neue"',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  palette: {
    type: 'dark',

    primary: {
      main: '#FFFFFF',
    },

    // background: {
    //   // default: "#244E60",
    // }
  },
});

function ThemeDark({ children }) {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
}

ThemeDark.propTypes = {
  children: PropTypes.node,
};

export default memo(ThemeDark);
