import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
  },
  item: {
    padding: theme.spacing(2), // button has default margin
  },
  right: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
}));

export { useStyles };
