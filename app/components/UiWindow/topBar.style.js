import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
    backgroundColor: theme.palette.primary.dark,
    color: 'white',
  },
  forDialog: {
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
    color: 'tael',
    // backgroundColor: theme.palette.primary.dark,
  },
  item: {
    padding: theme.spacing(2),
  },
  right: {
    display: 'flex',
    justifyContent: 'flex-end',
    paddingRight: theme.spacing(4),
  },
}));

export { useStyles };
