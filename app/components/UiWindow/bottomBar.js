import React, { memo } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Divider, Grid } from '@material-ui/core';

import { useStyles } from './bottomBar.style';

function UiWindowBottomBar({ primary, secondary }) {
  const classes = useStyles();
  if (!(Boolean(primary) && Boolean(secondary))) {
    return null;
  }
  return (
    <div className={classes.root}>
      <Divider />
      <Grid container>
        <Grid item sm={6} className={classes.item}>
          {primary}
        </Grid>
        <Grid item sm={6} className={clsx(classes.item, classes.right)}>
          {secondary}
        </Grid>
      </Grid>
    </div>
  );
}

UiWindowBottomBar.propTypes = {
  primary: PropTypes.any,
  secondary: PropTypes.any,
};

export default memo(UiWindowBottomBar);
