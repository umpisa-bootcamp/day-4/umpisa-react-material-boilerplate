/**
 *
 * Asynchronously loads the component for UiWindow
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
