/**
 *
 * UiWindow
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Paper } from '@material-ui/core';
import UiWindowTopBar from './topBar';
import UiWindowBottomBar from './bottomBar';

import { useStyles } from './index.style';

function UiWindow({
  action,
  children,
  middleAction,
  title,
  bottomPrimaryAction,
  bottomSecondaryAction,
  isForDialog,
  onClose,
}) {
  const classes = useStyles();
  return (
    <Paper className={classes.root}>
      <UiWindowTopBar
        title={title}
        middleAction={middleAction}
        action={action}
        isDialog={isForDialog}
        onClose={onClose} // if for dialog
      />
      {children}
      <UiWindowBottomBar
        primary={bottomPrimaryAction}
        secondary={bottomSecondaryAction}
      />
    </Paper>
  );
}

UiWindow.propTypes = {
  children: PropTypes.node,
  title: PropTypes.any,
  middleAction: PropTypes.any,
  action: PropTypes.any,
  bottomPrimaryAction: PropTypes.any,
  bottomSecondaryAction: PropTypes.any,
  onClose: PropTypes.func,
  isForDialog: PropTypes.bool,
};

export default memo(UiWindow);
