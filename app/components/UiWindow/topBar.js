import React, { memo } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Divider, Grid, IconButton } from '@material-ui/core';
import { Close } from '@material-ui/icons';

import { useStyles } from './topBar.style';
import { non } from '../../utils/function';

function UiWindowTopBar({
  title,
  middleAction,
  action,
  isDialog,
  onClose = non,
}) {
  const classes = useStyles();

  return (
    <div className={isDialog ? classes.forDialog : classes.root}>
      <Grid container>
        <Grid item sm={5} className={classes.item}>
          {title}
        </Grid>
        <Grid item sm={4} className={classes.item}>
          {middleAction}
        </Grid>
        <Grid item sm={3} className={clsx(classes.item, classes.right)}>
          {isDialog ? (
            <IconButton size="small" onClick={onClose}>
              <Close />
            </IconButton>
          ) : (
            action
          )}
        </Grid>
      </Grid>
      <Divider />
    </div>
  );
}

UiWindowTopBar.propTypes = {
  title: PropTypes.any,
  middleAction: PropTypes.any,
  action: PropTypes.any,
  onClose: PropTypes.func,
  isDialog: PropTypes.bool,
};

export default memo(UiWindowTopBar);
