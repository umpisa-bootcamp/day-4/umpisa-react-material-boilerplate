/**
 *
 * UiOptionDisplay
 *
 */

import { memo } from 'react';
import PropTypes from 'prop-types';

function UiOptionDisplay({ condition, truely, falsy }) {
  if (condition) {
    return truely;
  }
  return falsy;
}

UiOptionDisplay.propTypes = {
  condition: PropTypes.bool.isRequired,
  truely: PropTypes.node.isRequired,
  falsy: PropTypes.node.isRequired,
};

export default memo(UiOptionDisplay);
