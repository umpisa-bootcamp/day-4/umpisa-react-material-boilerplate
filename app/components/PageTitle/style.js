import { makeStyles } from '@material-ui/core/styles';
import { themeColor, grey1 } from '../../constants/color';

export const useStyles = makeStyles(() => ({
  before: {
    fontFamily: 'Roboto',
    fontSize: '18px',
    lineHeight: '20px',
    color: grey1,
    fontWeight: 300,
  },
  label: {
    fontFamily: 'Roboto',
    fontSize: '18px',
    lineHeight: '20px',
    color: themeColor,
  },
}));
