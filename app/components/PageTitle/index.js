/**
 *
 * PageTitle
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Typography, Box } from '@material-ui/core';

import { useStyles } from './style';

function PageTitle({ title, before }) {
  const classes = useStyles();

  return (
    <Box
      display="flex"
      flexDirection="row"
      justifyContent="flex-start"
      alignItems="center"
    >
      <Typography className={classes.before}>{before}</Typography>
      <Typography className={classes.label}>
        {before && (
          <svg
            width="32"
            height="32"
            viewBox="0 0 32 32"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M11.4533 23.12L17.56 17L11.4533 10.88L13.3333 9L21.3333 17L13.3333 25L11.4533 23.12Z"
              fill="#244E60"
            />
            <mask
              id="mask0"
              mask-type="alpha"
              maskUnits="userSpaceOnUse"
              x="11"
              y="9"
              width="11"
              height="16"
            >
              <path
                d="M11.4533 23.12L17.56 17L11.4533 10.88L13.3333 9L21.3333 17L13.3333 25L11.4533 23.12Z"
                fill="white"
              />
            </mask>
            <g mask="url(#mask0)">
              <rect width="32" height="32" fill="#244E60" />
            </g>
          </svg>
        )}
        {title}
      </Typography>
    </Box>
  );
}

PageTitle.propTypes = {
  title: PropTypes.string.isRequired,
  before: PropTypes.string,
};

export default memo(PageTitle);
