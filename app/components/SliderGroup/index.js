/**
 *
 * SliderGroup
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Slider, Typography } from '@material-ui/core';

function SliderGroup({ start, end, marks = [], onChange, label }) {
  return (
    <div>
      <Typography gutterBottom>{label}</Typography>
      <Slider
        style={{ width: 300, marginTop: 20 }}
        track="inverted"
        marks={marks}
        step={null}
        onChange={onChange}
        value={[start, end]}
      />
    </div>
  );
}

SliderGroup.propTypes = {
  start: PropTypes.number,
  end: PropTypes.number,
  marks: PropTypes.array,
  onChange: PropTypes.func,
  label: PropTypes.string,
};

export default memo(SliderGroup);
