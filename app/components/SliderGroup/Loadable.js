/**
 *
 * Asynchronously loads the component for SliderGroup
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
