/**
 *
 * DateButtonsFilter
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Button, ButtonGroup } from '@material-ui/core';
import moment from 'moment';
import { dateFormat } from '../../constants/date';

const filters = [
  {
    label: '1M',
    getDateRange: () => ({
      start: moment()
        .subtract(1, 'months')
        .format(dateFormat),
      end: moment().format(dateFormat),
      value: '1M',
    }),
  },
  {
    label: '3M',
    getDateRange: () => ({
      start: moment()
        .subtract(3, 'months')
        .format(dateFormat),
      end: moment().format(dateFormat),
      value: '3M',
    }),
  },
  {
    label: '6M',
    getDateRange: () => ({
      start: moment()
        .subtract(6, 'months')
        .format(dateFormat),
      end: moment().format(dateFormat),
      value: '6M',
    }),
  },
  {
    label: '1Y',
    getDateRange: () => ({
      start: moment()
        .subtract(1, 'years')
        .format(dateFormat),
      end: moment().format(dateFormat),
      value: '1Y',
    }),
  },
  {
    label: 'LASTY',
    getDateRange: () => ({
      start: moment()
        .startOf('year')
        .format(dateFormat),
      end: moment().format(dateFormat),
      value: 'LASTY',
    }),
  },
];

const activeColor = '#2F80ED';

function DateButtonsFilter({ selectedDateRange = '1M', onButtonSelect }) {
  const activeButtonStyle = { color: activeColor };
  return (
    <ButtonGroup size="small">
      {filters.map(elem => (
        <Button
          style={selectedDateRange === elem.label ? activeButtonStyle : {}}
          onClick={() => onButtonSelect(elem.getDateRange())}
          key={elem.label}
        >
          {elem.label}
        </Button>
      ))}
    </ButtonGroup>
  );
}

DateButtonsFilter.propTypes = {
  selectedDateRange: PropTypes.string,
  onButtonSelect: PropTypes.func.isRequired,
};

export default memo(DateButtonsFilter);
