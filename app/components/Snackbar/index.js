/**
 *
 * SnackBar
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import MuiAlert from '@material-ui/lab/Alert';
import MuiSnackBar from '@material-ui/core/Snackbar';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SnackBar({ open = false, handleClose, severity, children, ...props }) {
  const onNonClickAway = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    handleClose();
  };

  return (
    <MuiSnackBar
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      open={open}
      autoHideDuration={6000}
      onClose={onNonClickAway}
      {...props}
    >
      <Alert onClose={onNonClickAway} severity={severity || 'info'}>
        {children}
      </Alert>
    </MuiSnackBar>
  );
}

SnackBar.propTypes = {
  open: PropTypes.bool,
  handleClose: PropTypes.func,
  children: PropTypes.node,
  severity: PropTypes.oneOf(['success', 'info', 'error']),
};

export default memo(SnackBar);
