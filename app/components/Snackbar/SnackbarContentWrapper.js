import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import WarningIcon from '@material-ui/icons/Warning';
import { useStyles } from './style';

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

function SnackbarContentWrapper(props) {
  const classes = useStyles();
  const {
    className,
    message,
    onClose,
    variant,
    linkText,
    linkUrl,
    ...other
  } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      onClose={onClose}
      className={clsx(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={clsx(classes.icon, classes.iconVariant)} />
          {message} &nbsp;
          <a href={linkUrl} className={classes.link}>
            {linkText}
          </a>
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="close"
          color="inherit"
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

SnackbarContentWrapper.propTypes = {
  className: PropTypes.string,
  message: PropTypes.string,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['error', 'info', 'success', 'warning']).isRequired,
  linkText: PropTypes.string,
  linkUrl: PropTypes.string,
};

export default SnackbarContentWrapper;
