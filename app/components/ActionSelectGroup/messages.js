/*
 * ActionSelectGroup Messages
 *
 * This contains all the text for the ActionSelectGroup component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.ActionSelectGroup';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ActionSelectGroup component!',
  },
});
