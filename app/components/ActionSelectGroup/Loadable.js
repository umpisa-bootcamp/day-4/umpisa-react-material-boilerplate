/**
 *
 * Asynchronously loads the component for ActionSelectGroup
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
