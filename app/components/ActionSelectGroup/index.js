/**
 *
 * ActionSelectGroup
 *
 */

import React, { Fragment, memo, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  MenuItem,
  FormControlLabel,
  TextField,
  Typography,
} from '@material-ui/core';
import { useStyles } from './style';

const findActionByKey = (actions, key) =>
  actions.find(action => action.key === key);

function ActionSelectGroup({ actions = [], isLoading = false }) {
  const classes = useStyles();

  const [selectedAction, setSelectedAction] = useState('');

  const onSubmit = () => {
    const action = findActionByKey(actions, selectedAction);
    action.submit();
    setSelectedAction('');
  };

  return (
    <Fragment>
      <FormControlLabel
        className={classes.labelGroup}
        control={
          <TextField
            className={classes.labelGroupSelect}
            margin="dense"
            label="select action"
            select
            variant="outlined"
            value={selectedAction}
            onChange={e => setSelectedAction(e.target.value)}
          >
            <MenuItem value="">
              <em>---------</em>
            </MenuItem>
            {actions.map(action => (
              <MenuItem value={action.key} key={action.key}>
                <em>{action.label}</em>
              </MenuItem>
            ))}
          </TextField>
        }
        label={<Typography className={classes.label}>Actions:</Typography>}
        labelPlacement="start"
      />
      <Button disabled={selectedAction === '' || isLoading} onClick={onSubmit}>
        <Typography>Go</Typography>
      </Button>
    </Fragment>
  );
}

ActionSelectGroup.propTypes = {
  actions: PropTypes.array,
  isLoading: PropTypes.bool,
};

export default memo(ActionSelectGroup);
