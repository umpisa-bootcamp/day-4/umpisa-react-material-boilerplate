import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  labelGroup: {
    marginLeft: 0,
    marginRight: theme.spacing(2),
  },
  labelGroupSelect: {
    minWidth: 200,
  },
  label: {
    marginRight: theme.spacing(2),
  },
}));
