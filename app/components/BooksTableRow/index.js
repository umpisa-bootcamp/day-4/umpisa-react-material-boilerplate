/**
 *
 * BooksTableRow
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

function BooksTableRow({ book, index, onClickRow }) {
  return (
    <tr key={`tr_${index}`} onClick={() => onClickRow(book)}>
      <td>{book.title}</td>
      <td>{book.description}</td>
    </tr>
  );
}

BooksTableRow.propTypes = {
  book: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  onClickRow: PropTypes.func.isRequired,
};

export default BooksTableRow;
