/**
 *
 * TabBar
 *
 */

import React, { memo } from 'react';
import { Paper, Tab, Tabs } from '@material-ui/core';
import PropTypes from 'prop-types';

function TabBar({ activeTab, tabs = [], onChange }) {
  return (
    <Paper square>
      <Tabs
        variant="fullWidth"
        value={activeTab}
        indicatorColor="primary"
        textColor="primary"
        onChange={(_, value) => onChange(value)}
      >
        {tabs.map(tab => (
          <Tab label={`${tab.label}`} key={tab.label} />
        ))}
      </Tabs>
    </Paper>
  );
}

TabBar.propTypes = {
  activeTab: PropTypes.number,
  tabs: PropTypes.array,
  onChange: PropTypes.func.isRequired,
};

export default memo(TabBar);
