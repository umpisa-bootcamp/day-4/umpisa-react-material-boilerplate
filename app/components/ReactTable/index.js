/**
 *
 * ReactTable
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { useTable, usePagination, useGlobalFilter } from 'react-table';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import SearchBar from '../SearchBar';

import { useStyles, StyledTableRow } from './style';

function GlobalFilter({ globalFilter, setGlobalFilter }) {
  return (
    <span>
      <SearchBar
        onChange={e => {
          setGlobalFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
        }}
        value={globalFilter || ''}
      />
    </span>
  );
}

GlobalFilter.propTypes = {
  globalFilter: PropTypes.string,
  setGlobalFilter: PropTypes.func,
};

function ReactTable({ columns = [], data = [] }) {
  const {
    getTableProps,
    headerGroups,
    // rows,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    preGlobalFilteredRows,
    setGlobalFilter,
    state,
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 },
      searchInput: '',
    },
    useGlobalFilter,
    usePagination,
  );

  const classes = useStyles();

  // Render the UI for your table
  return (
    <div>
      <div style={{ textAlign: 'right' }}>
        <GlobalFilter
          preGlobalFilteredRows={preGlobalFilteredRows}
          globalFilter={state.globalFilter}
          setGlobalFilter={setGlobalFilter}
        />
      </div>

      <Paper className={classes.paper}>
        <div className={classes.wrapper}>
          <div className={classes.content}>
            <Table {...getTableProps()}>
              <TableHead>
                {headerGroups.map(headerGroup => (
                  <TableRow {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map(column => (
                      <TableCell
                        className={classes.tableCell}
                        {...column.getHeaderProps()}
                      >
                        <h5 variant="h5" className={classes.tableHeadTitle}>
                          {column.render('Header')}
                        </h5>
                      </TableCell>
                    ))}
                  </TableRow>
                ))}
              </TableHead>
              <TableBody>
                {page.map(row => {
                  prepareRow(row);
                  return (
                    <StyledTableRow
                      className={classes.tableRow}
                      {...row.getRowProps()}
                    >
                      {row.cells.map(cell => (
                        <TableCell {...cell.getCellProps()}>
                          {cell.render('Cell')}
                        </TableCell>
                      ))}
                    </StyledTableRow>
                  );
                })}
              </TableBody>
            </Table>
          </div>
        </div>
        <div className="pagination" style={{ marginTop: 20, marginLeft: 20 }}>
          <button
            onClick={() => gotoPage(0)}
            disabled={!canPreviousPage}
            type="button"
          >
            {'<<'}
          </button>{' '}
          <button
            onClick={() => previousPage()}
            disabled={!canPreviousPage}
            type="button"
          >
            {'<'}
          </button>{' '}
          <button
            onClick={() => nextPage()}
            disabled={!canNextPage}
            type="button"
          >
            {'>'}
          </button>{' '}
          <button
            onClick={() => gotoPage(pageCount - 1)}
            disabled={!canNextPage}
            type="button"
          >
            {'>>'}
          </button>{' '}
          <span>
            Page{' '}
            <strong>
              {state.pageIndex + 1} of {pageOptions.length}
            </strong>{' '}
          </span>
          <select
            value={state.pageSize}
            onChange={e => {
              setPageSize(Number(e.target.value));
            }}
          >
            {[10, 20, 30, 40, 50].map(size => (
              <option key={size} value={size}>
                Show {size}
              </option>
            ))}
          </select>
        </div>
      </Paper>
    </div>
  );
}

ReactTable.propTypes = {
  columns: PropTypes.array,
  data: PropTypes.array,
};

export default memo(ReactTable);
