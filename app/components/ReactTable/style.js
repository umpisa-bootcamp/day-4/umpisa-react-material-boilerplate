import { makeStyles, withStyles } from '@material-ui/core/styles';
import { TableRow } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    // width: '100%',
  },
  content: {
    minWidth: 1200,
  },
  wrapper: {
    overflowX: 'auto',
  },

  paper: {
    borderRadius: 0,
    paddingBottom: 20,
    //   marginTop: theme.spacing(3),
    //   width: '100%',
    //   overflowX: 'auto',
    //   marginBottom: theme.spacing(2),
  },
  title: {
    // fontSize: 39,
    fontWeight: 'bold',
  },
  table: {
    minWidth: 650,
  },
  tableHead: {
    borderBottom: '1px solid #BDBDBD',
  },
  tableRow: {
    // borderBottom: '1px solid ' + theme.palette.primary.main,
  },
  tableHeadTitle: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 18,
    textTransform: 'uppercase',
    marginTop: 5,
    marginBottom: 5,
  },
  tableCell: {
    padding: theme.spacing(1),
    fontSize: 12,
    borderBottomWidth: 0,
  },
  checkboxCell: {
    width: 30,
    padding: theme.spacing(1),
    borderBottomWidth: 0,
  },
  checkbox: {
    color: theme.palette.primary.main,
    margin: 0,
    padding: 0,
  },
}));

const StyledTableRow = withStyles(() => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: '#F2F2F2',
    },
  },
}))(TableRow);

export { StyledTableRow, useStyles };
