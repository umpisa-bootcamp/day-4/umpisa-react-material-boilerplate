/**
 *
 * Asynchronously loads the component for DateFilters
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
