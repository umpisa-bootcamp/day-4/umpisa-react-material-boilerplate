import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  endDate: {
    marginLeft: theme.spacing(1),
  },
}));
