import { makeStyles } from '@material-ui/styles';
import { themeColor } from 'constants/color';

export const useStyles = makeStyles(() => ({
  paginationContainer: {
    display: 'flex',
    justifyContent: 'center',
    color: themeColor,
  },
  pagination: {
    display: 'flex',
    justifyContent: 'space-between',
    '& li': {
      listStyleType: 'none',
      marginLeft: '8px',
      cursor: 'pointer',
    },
  },
  active: {
    fontWeight: '500',
  },
}));
