/**
 *
 * Pagination
 *
 */

import React, { memo, useCallback } from 'react';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';
import { non } from 'utils/function';
import { useStyles } from './style';

export const usePagination = (onChangeState = non) => {
  const onPageChange = useCallback(
    data => {
      const newPage = Number(data.selected) + 1;
      if (newPage >= 1) {
        onChangeState(newPage);
      }
    },
    [onChangeState],
  );
  return onPageChange;
};

function Pagination({ totalPage, onPageChange = non, ...props }) {
  const classes = useStyles();
  return (
    <div className={classes.paginationContainer}>
      <ReactPaginate
        previousLabel="Prev"
        nextLabel="Next"
        breakLabel="..."
        pageCount={totalPage}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={onPageChange}
        containerClassName={classes.pagination}
        subContainerClassName="pages pagination"
        activeClassName={classes.active}
        {...props}
      />
    </div>
  );
}

Pagination.propTypes = {
  totalPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
};

export default memo(Pagination);
