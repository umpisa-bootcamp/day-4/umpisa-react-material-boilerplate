/**
 *
 * ImagesDialog
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogContent,
  DialogTitle,
  DialogActions,
  Button,
} from '@material-ui/core';
import ImageGallery from 'react-image-gallery';

function ImagesDialog({ open = false, onClose, images = [] }) {
  return (
    <Dialog
      open={open}
      onClose={onClose}
      maxWidth="sm"
      aria-labelledby="form-dialog-title"
    >
      <DialogContent>
        <DialogTitle>Attachments</DialogTitle>
        {images.length > 0 && (
          <ImageGallery items={images} showPlayButton={false} />
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
}

ImagesDialog.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  images: PropTypes.arrayOf(
    PropTypes.shape({
      original: PropTypes.string.isRequired,
      thumbnail: PropTypes.string.isRequired,
    }),
  ),
};

export default memo(ImagesDialog);
