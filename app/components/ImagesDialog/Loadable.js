/**
 *
 * Asynchronously loads the component for ImagesDialog
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
