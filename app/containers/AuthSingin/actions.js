/*
 *
 * AuthSingin actions
 *
 */

import {
  DEFAULT_ACTION,
  AUTH_SIGNIN_LOADING,
  AUTH_SIGNIN_ERROR,
} from './constants';

export function authSinginLoading(isLoading = true) {
  return {
    type: AUTH_SIGNIN_LOADING,
    payload: isLoading,
  };
}

export function authSinginError(message) {
  return {
    type: AUTH_SIGNIN_ERROR,
    payload: message,
  };
}

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
