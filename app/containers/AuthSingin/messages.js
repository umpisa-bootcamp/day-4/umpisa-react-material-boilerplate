/*
 * AuthSingin Messages
 *
 * This contains all the text for the AuthSingin container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.AuthSingin';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the AuthSingin container!',
  },
});
