import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the authSingin state domain
 */

const selectAuthSinginDomain = state => state.authSingin || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by AuthSingin
 */

const makeSelectAuthSingin = () =>
  createSelector(
    selectAuthSinginDomain,
    substate => substate,
  );

export default makeSelectAuthSingin;
export { selectAuthSinginDomain };
