/**
 *
 * AuthSingin
 *
 */

import React, { memo, useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import Container from '@material-ui/core/Container';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { Button, Typography } from '@material-ui/core';
import makeSelectAuthSingin from './selectors';
import reducer from './reducer';
import saga from './saga';
import makeSelectAuth from '../Auth/selectors';
import { authLogin } from '../Auth/actions';
import { splashScreen, adminScreen } from '../../constants/route';
import AuthSiginForm from '../../components/AuthSiginForm';
import { getRecords } from '../../utils/helper/modelHelper';

export function AuthSingin({ history, onLogin, authSingin }) {
  useInjectReducer({ key: 'authSingin', reducer });
  useInjectSaga({ key: 'authSingin', saga });

  const loginWithSuccess = useCallback((username, password) => {
    onLogin(username, password, {
      onSuccess: user => {
        if (user.isAdmin) {
          return history.push(adminScreen);
        }

        return history.push(splashScreen);
      },
    });
  });

  return (
    <div>
      <Helmet>
        <title>AuthSingin</title>
        <meta name="description" content="Description of AuthSingin" />
      </Helmet>
      <Container>
        <AuthSiginForm
          onLogin={loginWithSuccess}
          isLoading={authSingin.isLoading}
          error={authSingin.error}
        />
      </Container>
    </div>
  );
}

AuthSingin.propTypes = {
  // auth: PropTypes.object, // redux
  authSingin: PropTypes.object, // redux
  onLogin: PropTypes.func.isRequired, // redux
  history: PropTypes.object.isRequired, // from Route
};

const mapStateToProps = createStructuredSelector({
  authSingin: makeSelectAuthSingin(),
  auth: makeSelectAuth(),
});

const mapDispatchToProps = {
  onLogin: authLogin,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(AuthSingin);
