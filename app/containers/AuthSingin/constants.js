/*
 *
 * AuthSingin constants
 *
 */

export const DEFAULT_ACTION = 'app/AuthSingin/DEFAULT_ACTION';
export const AUTH_SIGNIN_ERROR = 'app/AuthSingin/ERROR';
export const AUTH_SIGNIN_LOADING = 'app/AuthSingin/loading';
