/**
 *
 * Asynchronously loads the component for AuthSingin
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
