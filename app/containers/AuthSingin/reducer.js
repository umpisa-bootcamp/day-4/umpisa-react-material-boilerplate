/*
 *
 * AuthSingin reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  AUTH_SIGNIN_LOADING,
  AUTH_SIGNIN_ERROR,
} from './constants';

export const initialState = {
  isLoading: false,
  error: '',
};

/* eslint-disable default-case, no-param-reassign */
const authSinginReducer = (state = initialState, { type, payload }) =>
  produce(state, draft => {
    switch (type) {
      case AUTH_SIGNIN_LOADING:
        draft.isLoading = payload;
        break;
      case AUTH_SIGNIN_ERROR:
        draft.error = payload;
        break;
      case DEFAULT_ACTION:
        break;
    }
  });

export default authSinginReducer;
