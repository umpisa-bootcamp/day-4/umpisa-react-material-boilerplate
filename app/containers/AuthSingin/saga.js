import { call, put, takeLatest } from 'redux-saga/effects';
import { LOGIN } from '../Auth/constants';
import request from '../../utils/request';
import { authLogin } from '../../constants/api';
import { authSuccess } from '../Auth/actions';
import { authSinginLoading, authSinginError } from './actions';
import { save, storageNames } from '../../utils/persitedStore';

/**
 * Github repos request/response handler
 */
export function* postLogin({ payload }) {
  try {
    const { options, email, password } = payload;
    yield put(authSinginLoading(true));

    const result = yield call(request, authLogin, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email, password }),
    });
    // persist data
    // result = { user, auth };
    save(storageNames.auth, result); // flash only no need to wait

    // put user data to global state
    yield put(authSuccess(result));
    yield put(authSinginError('')); // clean errors

    // handle if user dont have type
    if (!result.user.role) {
      throw new Error(`${email} dont have a user type`);
    }

    if (options && options.onSuccess) {
      options.onSuccess(result.user);
    }
  } catch (err) {
    // yield put(authSinginError(err.message));
    yield put(authSinginError('Email Address and/or Password is incorrect.')); // must be fix on api error message
  } finally {
    yield put(authSinginLoading(false));
  }
}

export default function* authSinginSaga() {
  // will be using global reducer for auth not scope reducer
  yield takeLatest(LOGIN, postLogin);
}
