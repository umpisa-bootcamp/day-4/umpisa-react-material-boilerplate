import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the booksCreate state domain
 */

const selectBooksCreateDomain = state => state.booksCreate || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by BooksCreate
 */

const makeSelectBooksCreate = () =>
  createSelector(
    selectBooksCreateDomain,
    substate => substate,
  );

export { selectBooksCreateDomain, makeSelectBooksCreate };
