/*
 *
 * BooksCreate actions
 *
 */

import {
  BOOKS_CREATE_MERGE,
  BOOKS_CREATE_FETCH,
  BOOKS_CREATE_SAVE,
} from './constants';

export function booksCreateMerge(payload = {}) {
  return {
    type: BOOKS_CREATE_MERGE,
    payload,
  };
}

export function booksCreateFetch(payload = {}) {
  return {
    type: BOOKS_CREATE_FETCH,
    payload,
  };
}

export function booksCreateSave(payload = {}) {
  return {
    type: BOOKS_CREATE_SAVE,
    payload,
  };
}

export function booksCreateSetLoading(state = true) {
  return {
    type: BOOKS_CREATE_MERGE,
    payload: {
      isLoading: state || false,
    },
  };
}

export function booksCreateSetMessage(message = '') {
  return {
    type: BOOKS_CREATE_MERGE,
    payload: { message },
  };
}
