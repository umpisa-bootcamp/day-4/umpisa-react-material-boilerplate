/**
 *
 * Asynchronously loads the component for BooksCreate
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
