/**
 *
 * BooksCreate
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { Grid, TextField, Button } from '@material-ui/core';
import reducer from './reducer';

import composer from './composer';

import saga from './saga';

export function BooksCreate({ onBooksCreateSave, history }) {
  useInjectReducer({ key: 'booksCreate', reducer });
  useInjectSaga({ key: 'booksCreate', saga });

  const [state, setState] = useState({
    title: '',
    description: '',
  });

  const onChangeField = (event, field) => {
    setState({
      ...state,
      [field]: event.target.value,
    });
  };

  const onSubmit = () => {
    onBooksCreateSave({
      params: state,
      onSuccess: () => {
        history.push('/dashboard');
      },
    });
  };

  return (
    <div>
      <Helmet>
        <title>BooksCreate</title>
        <meta name="description" content="Description of BooksCreate" />
      </Helmet>

      <Grid container direction="column">
        <Grid>
          <TextField label="Title" onChange={e => onChangeField(e, 'title')} />
        </Grid>
        <Grid>
          <TextField
            label="Title"
            onChange={e => onChangeField(e, 'description')}
          />
        </Grid>
        <Grid>
          <Button onClick={onSubmit}>Save</Button>
        </Grid>
      </Grid>
    </div>
  );
}

BooksCreate.propTypes = {
  //  booksCreate: PropTypes.object.isRequired,
  //  onBooksCreateMerge: PropTypes.func.isRequired,
  //
  //  onBooksCreateFetch: PropTypes.func.isRequired,
  onBooksCreateSave: PropTypes.func.isRequired,
  history: PropTypes.object,
};

export default composer(BooksCreate);
