import { memo } from 'react';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { booksCreateFetch, booksCreateSave, booksCreateMerge } from './actions';

import { makeSelectBooksCreate } from './selectors';

const mapStateToProps = createStructuredSelector({
  booksCreate: makeSelectBooksCreate(),
});

const mapDispatchToProps = {
  onBooksCreateMerge: booksCreateMerge,

  onBooksCreateFetch: booksCreateFetch,
  onBooksCreateSave: booksCreateSave,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
);
