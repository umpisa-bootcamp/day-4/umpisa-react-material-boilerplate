/*
 *
 * BooksCreate constants
 *
 */

export const BOOKS_CREATE_MERGE = 'app/BooksCreate/BOOKS_CREATE_MERGE';

export const BOOKS_CREATE_FETCH = 'app/BooksCreate/BOOKS_CREATE_FETCH';
export const BOOKS_CREATE_SAVE = 'app/BooksCreate/BOOKS_CREATE_SAVE';
