import { takeLatest } from 'redux-saga/effects';
import { BOOKS_CREATE_FETCH, BOOKS_CREATE_SAVE } from '../constants';
import fetch from './fetch.saga';
import save from './save.saga';

export default function* booksCreateSaga() {
  yield takeLatest(BOOKS_CREATE_FETCH, fetch);
  yield takeLatest(BOOKS_CREATE_SAVE, save);
}
