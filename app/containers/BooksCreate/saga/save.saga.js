import { call, put } from 'redux-saga/effects';
import QueryString from 'query-string';

import * as api from '../../../constants/api';
import * as request from '../../../utils/request';
import { booksCreateSetLoading, booksCreateMerge } from '../actions';

export default function* save(action) {
  const {
    params = {},
    query = {},
    onSuccess = () => {},
    onError = () => {},
  } = action.payload;

  try {
    yield put(booksCreateSetLoading(true));
    const queryString = QueryString.stringify(query, { skipNull: true });
    const url = `${api.booksList}?${queryString}`;

    const { data } = yield call(request.post, url, params);

    yield put(
      booksCreateMerge({
        item: data,
        isLoading: false,
        isUpdated: true,
      }),
    );
    yield call(onSuccess, data);
  } catch (err) {
    yield call(onError, err.message);
  } finally {
    yield put(booksCreateSetLoading(false));
  }
}
