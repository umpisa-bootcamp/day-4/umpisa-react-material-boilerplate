import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the sampleCantainer state domain
 */

const selectSampleCantainerDomain = state =>
  state.sampleCantainer || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by SampleCantainer
 */

const makeSelectSampleCantainer = () =>
  createSelector(
    selectSampleCantainerDomain,
    substate => substate,
  );

export { selectSampleCantainerDomain, makeSelectSampleCantainer };
