/**
 *
 * Asynchronously loads the component for SampleCantainer
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
