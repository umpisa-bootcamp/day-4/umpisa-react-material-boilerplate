/*
 *
 * SampleCantainer actions
 *
 */

import {
  SAMPLE_CANTAINER_MERGE,
  SAMPLE_CANTAINER_FETCH,
  SAMPLE_CANTAINER_SAVE,
} from './constants';

export function sampleCantainerMerge(payload = {}) {
  return {
    type: SAMPLE_CANTAINER_MERGE,
    payload,
  };
}

export function sampleCantainerFetch(payload = {}) {
  return {
    type: SAMPLE_CANTAINER_FETCH,
    payload,
  };
}

export function sampleCantainerSave(payload = {}) {
  return {
    type: SAMPLE_CANTAINER_SAVE,
    payload,
  };
}

export function sampleCantainerSetLoading(state = true) {
  return {
    type: SAMPLE_CANTAINER_MERGE,
    payload: {
      isLoading: state || false,
    },
  };
}

export function sampleCantainerSetMessage(message = '') {
  return {
    type: SAMPLE_CANTAINER_MERGE,
    payload: { message },
  };
}
