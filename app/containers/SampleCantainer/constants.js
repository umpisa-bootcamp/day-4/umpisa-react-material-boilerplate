/*
 *
 * SampleCantainer constants
 *
 */

export const SAMPLE_CANTAINER_MERGE =
  'app/SampleCantainer/SAMPLE_CANTAINER_MERGE';

export const SAMPLE_CANTAINER_FETCH =
  'app/SampleCantainer/SAMPLE_CANTAINER_FETCH';
export const SAMPLE_CANTAINER_SAVE =
  'app/SampleCantainer/SAMPLE_CANTAINER_SAVE';
