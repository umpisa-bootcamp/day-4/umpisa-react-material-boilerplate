import { call, put } from 'redux-saga/effects';

import * as api from '../../../constants/api';
import * as request from '../../../utils/request';

import { sampleCantainerSetLoading, sampleCantainerMerge } from '../actions';

export default function* fetch(action) {
  const { onSuccess = () => {}, onError = () => {} } = action.payload;

  try {
    yield put(sampleCantainerSetLoading(true));

    const result = yield call(request.get, api.user);
    // store data on redux state
    yield put(
      sampleCantainerMerge({
        records: result,
        isLoading: false,
        isUpdated: true,
      }),
    );
    yield call(onSuccess, result);
  } catch (err) {
    yield call(onError, err.message);
  } finally {
    yield put(sampleCantainerSetLoading(false));
  }
}
