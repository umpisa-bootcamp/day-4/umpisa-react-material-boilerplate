import { takeLatest } from 'redux-saga/effects';
import { SAMPLE_CANTAINER_FETCH, SAMPLE_CANTAINER_SAVE } from '../constants';
import fetch from './fetch.saga';
import save from './save.saga';

export default function* sampleCantainerSaga() {
  yield takeLatest(SAMPLE_CANTAINER_FETCH, fetch);
  yield takeLatest(SAMPLE_CANTAINER_SAVE, save);
}
