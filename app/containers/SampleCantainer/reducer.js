/*
 *
 * SampleCantainer reducer
 *
 */
import produce from 'immer';
import { merge } from '../../utils/object';
import { SAMPLE_CANTAINER_MERGE } from './constants';

export const initialState = {
  records: null,
  isLoading: true,
  isUpdated: false,
  error: '',
  message: '', // status messaging
};

/* eslint-disable default-case, no-param-reassign */
const sampleCantainerReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SAMPLE_CANTAINER_MERGE:
        return merge(draft, action.payload);
    }
    return draft;
  });

export default sampleCantainerReducer;
