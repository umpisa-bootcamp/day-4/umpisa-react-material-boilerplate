import { memo } from 'react';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import {
  sampleCantainerFetch,
  sampleCantainerSave,
  sampleCantainerMerge,
} from './actions';

import { makeSelectSampleCantainer } from './selectors';

const mapStateToProps = createStructuredSelector({
  sampleCantainer: makeSelectSampleCantainer(),
});

const mapDispatchToProps = {
  onSampleCantainerMerge: sampleCantainerMerge,

  onSampleCantainerFetch: sampleCantainerFetch,
  onSampleCantainerSave: sampleCantainerSave,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
);
