/**
 *
 * SampleCantainer
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import reducer from './reducer';

import composer from './composer';

import saga from './saga';

export function SampleCantainer() {
  useInjectReducer({ key: 'sampleCantainer', reducer });
  useInjectSaga({ key: 'sampleCantainer', saga });

  return (
    <div>
      <h1>SampleCantainer</h1>
    </div>
  );
}

SampleCantainer.propTypes = {
  //  sampleCantainer: PropTypes.object.isRequired,
  //  onSampleCantainerMerge: PropTypes.func.isRequired,
  //
  //  onSampleCantainerFetch: PropTypes.func.isRequired,
  //  onSampleCantainerSave: PropTypes.func.isRequired,
};

export default composer(SampleCantainer);
