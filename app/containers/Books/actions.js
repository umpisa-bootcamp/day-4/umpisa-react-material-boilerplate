/*
 *
 * Books actions
 *
 */

import { BOOKS_MERGE, BOOKS_FETCH, BOOKS_SAVE } from './constants';

export function booksMerge(payload = {}) {
  return {
    type: BOOKS_MERGE,
    payload,
  };
}

export function booksFetch(payload = {}) {
  return {
    type: BOOKS_FETCH,
    payload,
  };
}

export function booksSave(payload = {}) {
  return {
    type: BOOKS_SAVE,
    payload,
  };
}

export function booksSetLoading(state = true) {
  return {
    type: BOOKS_MERGE,
    payload: {
      isLoading: state || false,
    },
  };
}

export function booksSetMessage(message = '') {
  return {
    type: BOOKS_MERGE,
    payload: { message },
  };
}
