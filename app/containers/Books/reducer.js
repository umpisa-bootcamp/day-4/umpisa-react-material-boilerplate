/*
 *
 * Books reducer
 *
 */
import produce from 'immer';
import { merge } from '../../utils/object';
import { BOOKS_MERGE } from './constants';

export const initialState = {
  records: null,
  isLoading: false,
  isUpdated: false,
  error: '',
  message: '', // status messaging
  item: {},
  id: '', // active id for item
  queryString: '', // query string for api call
};

/* eslint-disable default-case, no-param-reassign */
const booksReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case BOOKS_MERGE:
        return merge(draft, action.payload);
    }
    return draft;
  });

export default booksReducer;
