/**
 *
 * Books
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import reducer from './reducer';

import composer from './composer';

import saga from './saga';

export function Books() {
  useInjectReducer({ key: 'books', reducer });
  useInjectSaga({ key: 'books', saga });

  return (
    <div>
      <Helmet>
        <title>Books</title>
        <meta name="description" content="Description of Books" />
      </Helmet>
    </div>
  );
}

Books.propTypes = {
  //  books: PropTypes.object.isRequired,
  //  onBooksMerge: PropTypes.func.isRequired,
  //
  //  onBooksFetch: PropTypes.func.isRequired,
  //  onBooksSave: PropTypes.func.isRequired,
};

export default composer(Books);
