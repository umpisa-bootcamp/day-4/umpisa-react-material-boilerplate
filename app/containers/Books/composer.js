import { memo } from 'react';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { booksFetch, booksSave, booksMerge } from './actions';

import { makeSelectBooks } from './selectors';

const mapStateToProps = createStructuredSelector({
  books: makeSelectBooks(),
});

const mapDispatchToProps = {
  onBooksMerge: booksMerge,

  onBooksFetch: booksFetch,
  onBooksSave: booksSave,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
);
