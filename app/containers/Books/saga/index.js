import { takeLatest } from 'redux-saga/effects';
import { BOOKS_FETCH, BOOKS_SAVE } from '../constants';
import fetch from './fetch.saga';
import save from './save.saga';

export default function* booksSaga() {
  yield takeLatest(BOOKS_FETCH, fetch);
  yield takeLatest(BOOKS_SAVE, save);
}
