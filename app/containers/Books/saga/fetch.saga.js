import { call, put, select } from 'redux-saga/effects';
import QueryString from 'query-string';

import * as api from '../../../constants/api';
import * as request from '../../../utils/request';

import { booksSetLoading, booksMerge } from '../actions';
import { makeSelectBooks } from '../selectors';

export default function* fetch(action) {
  const {
    query = {},
    onSuccess = () => {},
    onError = () => {},
  } = action.payload;

  try {
    yield put(booksSetLoading(true));
    const state = yield select(makeSelectBooks());

    const queryString = QueryString.stringify(query, { skipNull: true });
    const url = `${api.booksList}?${queryString || state.queryString}`;

    const { data } = yield call(request.get, url);

    yield put(
      booksMerge({
        records: data,
        isLoading: false,
        isUpdated: true,
        queryString,
      }),
    );
    yield call(onSuccess, data);
  } catch (err) {
    yield call(onError, err.message);
  } finally {
    yield put(booksSetLoading(false));
  }
}
