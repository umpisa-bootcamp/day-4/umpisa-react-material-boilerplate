import { call, put } from 'redux-saga/effects';
import QueryString from 'query-string';

import * as api from '../../../constants/api';
import * as request from '../../../utils/request';
import { booksSetLoading, booksMerge } from '../actions';

export default function* save(action) {
  const {
    params = {},
    query = {},
    onSuccess = () => {},
    onError = () => {},
  } = action.payload;

  try {
    yield put(booksSetLoading(true));
    const queryString = QueryString.stringify(query, { skipNull: true });
    const url = `${api.url}?${queryString}`;

    const { data } = yield call(request.put, url, params);

    yield put(
      booksMerge({
        item: data,
        isLoading: false,
        isUpdated: true,
      }),
    );
    yield call(onSuccess, data);
  } catch (err) {
    yield call(onError, err.message);
  } finally {
    yield put(booksSetLoading(false));
  }
}
