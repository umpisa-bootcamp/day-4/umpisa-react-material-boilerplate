/*
 *
 * Books constants
 *
 */

export const BOOKS_MERGE = 'app/Books/BOOKS_MERGE';

export const BOOKS_FETCH = 'app/Books/BOOKS_FETCH';
export const BOOKS_SAVE = 'app/Books/BOOKS_SAVE';
