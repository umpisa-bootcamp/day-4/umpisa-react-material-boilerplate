/**
 *
 * App
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import { withRouter } from 'react-router';

import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import LinearProgress from '@material-ui/core/LinearProgress';

import LayoutPrimary from 'components/LayoutPrimary';
import { AuthenticatedRoute } from 'components/ProtectedComponent';

import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { non } from 'utils/function';
import { userTypes } from 'constants/userType';
import useInterval from '@use-it/interval';
import AuthPage from '../Auth/Loadable';
import SplashPage from '../Splash/Loadable';
import DashboardPage from '../Dashboard/Loadable';
import AdminDashboardPage from '../AdminDashboard/Loadable';
import BooksCreatePage from '../BooksCreate/Loadable';
import BooksFormPage from '../BooksForm/Loadable';
import BooksPage from '../Books/Loadable';
import StoresPage from '../Store/Loadable';

import * as routes from '../../constants/route';
import makeSelectApp from './selectors';
import reducer from './reducer';
import saga from './saga';
import { appInitialize } from './actions';

import {
  makeSelectIsLogged,
  makeSelectUserType,
  makeSelectIsAdmin,
} from '../Auth/selectors';
import SampleCantainerPage from '../SampleCantainer';

export function App({
  app,
  onAppInitialize,
  isLogged,
  userType,
  isAdmin = false,
  ...props
}) {
  // used by all
  useInjectReducer({ key: 'app', reducer });
  useInjectSaga({ key: 'app', saga });

  useEffect(() => {
    onAppInitialize();

    if (isLogged) {
      // Sample here is notifications and messages
    }
  }, [isLogged]);

  useInterval(() => {
    if (isLogged) {
      // Sample here is notifications and messages
    }
  }, 60000);

  if (app.isLoading) {
    return <LinearProgress />;
  }
  return (
    <React.Fragment>
      <LayoutPrimary role={userType} {...props}>
        <Switch>
          <AuthenticatedRoute
            exact
            path="/dashboard"
            component={DashboardPage}
            isLogged={isLogged}
          />
          {/* <AuthenticatedRoute
            exact
            path={routes.books}
            component={BooksPage}
            isLogged={isLogged}
          /> */}
          <AuthenticatedRoute
            exact
            path="/books/create"
            component={BooksCreatePage}
            isLogged={isLogged}
          />
          <AuthenticatedRoute
            exact
            path="/books/form"
            component={BooksFormPage}
            isLogged={isLogged}
          />
          <Route exact path="/" component={SplashPage} />
          <Route path="/auth" component={AuthPage} />
          <Route path="/sample" component={SampleCantainerPage} />

          <Route path="/stores" component={StoresPage} />

          {/** add role later */}
          <AuthenticatedRoute
            path={routes.dashboard}
            component={AdminDashboardPage}
            isLogged={isLogged}
            role={userType}
            isAdmin={isAdmin}
            adminOnly
            allowedRoles={[userTypes.admin]}
          />
          <Route component={NotFoundPage} />
        </Switch>
      </LayoutPrimary>
    </React.Fragment>
  );
}

App.propTypes = {
  app: PropTypes.object, // redux
  onAppInitialize: PropTypes.func.isRequired, // redux
  isLogged: PropTypes.bool.isRequired,
  userType: PropTypes.string,
  isAdmin: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  app: makeSelectApp(),
  isLogged: makeSelectIsLogged(),
  userType: makeSelectUserType(),
  isAdmin: makeSelectIsAdmin(),
});

const mapDispatchToProps = {
  onAppInitialize: appInitialize,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withRouter,
  withConnect,
)(App);
