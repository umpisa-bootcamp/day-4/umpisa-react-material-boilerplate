import { call, put, takeLatest } from 'redux-saga/effects';
import { APP_INITIALIZE } from './constants';
import { appLoading } from './actions';
import { authSuccess } from '../Auth/actions';
import { load, storageNames } from '../../utils/persitedStore';

export function* initilize() {
  try {
    yield put(appLoading(true));
    // load persisted store and save to reducer
    const user = yield call(load, storageNames.auth);

    yield put(authSuccess(user));
  } catch (err) {
    // console.log(err);
  } finally {
    // console.log('appLoading');
    yield put(appLoading(false));
  }
}

export default function* appSaga() {
  yield takeLatest(APP_INITIALIZE, initilize);
}
