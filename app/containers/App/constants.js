/*
 *
 * App constants
 *
 */

export const DEFAULT_ACTION = 'app/App/DEFAULT_ACTION';
export const APP_LOADING = 'app/App/APP_LOADING';
export const APP_INITIALIZE = 'app/App/APP_INITIALIZE';
