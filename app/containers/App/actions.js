/*
 *
 * App actions
 *
 */

import { DEFAULT_ACTION, APP_INITIALIZE, APP_LOADING } from './constants';

export function appInitialize() {
  return {
    type: APP_INITIALIZE,
  };
}

export function appLoading(isLoading = true) {
  return {
    type: APP_LOADING,
    payload: isLoading,
  };
}

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
