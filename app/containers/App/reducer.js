/*
 *
 * App reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, APP_LOADING } from './constants';

export const initialState = {
  initialized: false, // used when all onload script finish loading
  isLoading: true, // when something needs to load
};

/* eslint-disable default-case, no-param-reassign */
const appReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case APP_LOADING:
        draft.isLoading = action.payload;
        break;
      case DEFAULT_ACTION:
        break;
    }
  });

export default appReducer;
