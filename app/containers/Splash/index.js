/**
 *
 * Splash
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectSplash from './selectors';
import reducer from './reducer';
import saga from './saga';
import { authSignin, adminScreen, dashboard } from '../../constants/route';
import makeSelectAuth from '../Auth/selectors';
import { userTypes } from '../../constants/userType';
import * as roles from '../../constants/role';

export function Splash({ history, auth }) {
  useInjectReducer({ key: 'splash', reducer });
  useInjectSaga({ key: 'splash', saga });
  if (!auth.isLogged) {
    history.push(authSignin);
    return null;
  }

  if (auth.user.role === userTypes.user) {
    history.push(dashboard);
    return null;
  }

  if (auth.user.role === userTypes.admin) {
    history.push(adminScreen);
    return null;
  }
  return <h1> already logged unknown user type</h1>;
}

Splash.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  auth: PropTypes.object,
  history: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  splash: makeSelectSplash(),
  auth: makeSelectAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Splash);
