/**
 *
 * Store
 *
 */

import React, { useEffect, useMemo } from 'react';
// import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import reducer from './reducer';

import composer from './composer';

import saga from './saga';

import UiTable from '../../components/UiTable';

export function Store({ onStoreFetch, onStoreSave, store }) {
  useInjectReducer({ key: 'store', reducer });
  useInjectSaga({ key: 'store', saga });

  useEffect(() => {
    onStoreFetch();
  }, []);

  const columns = useMemo(() => [
    { key: 'a1', name: 'Title', field: 'title' },
    { key: 'a2', name: 'Description', field: 'description' },
  ]);

  return (
    <div>
      <Helmet>
        <title>Store</title>
        <meta name="description" content="Description of Store" />
      </Helmet>
      <h1>Store</h1>

      <UiTable columns={columns} items={store.records} />
    </div>
  );
}

Store.propTypes = {
  //  store: PropTypes.object.isRequired,
  //  onStoreMerge: PropTypes.func.isRequired,
  //
  //  onStoreFetch: PropTypes.func.isRequired,
  //  onStoreSave: PropTypes.func.isRequired,
};

export default composer(Store);
