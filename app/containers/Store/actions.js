/*
 *
 * Store actions
 *
 */

import { STORE_MERGE, STORE_FETCH, STORE_SAVE } from './constants';

export function storeMerge(payload = {}) {
  return {
    type: STORE_MERGE,
    payload,
  };
}

export function storeFetch(payload = {}) {
  return {
    type: STORE_FETCH,
    payload,
  };
}

export function storeSave(payload = {}) {
  return {
    type: STORE_SAVE,
    payload,
  };
}

export function storeSetLoading(state = true) {
  return {
    type: STORE_MERGE,
    payload: {
      isLoading: state || false,
    },
  };
}

export function storeSetMessage(message = '') {
  return {
    type: STORE_MERGE,
    payload: { message },
  };
}
