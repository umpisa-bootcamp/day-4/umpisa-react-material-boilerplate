import { call, put, select } from 'redux-saga/effects';
import QueryString from 'query-string';

import * as api from '../../../constants/api';
import * as request from '../../../utils/request';

import { storeSetLoading, storeMerge } from '../actions';
import { makeSelectStore } from '../selectors';

export default function* fetch(action) {
  const {
    query = {},
    onSuccess = () => {},
    onError = () => {},
  } = action.payload;

  try {
    yield put(storeSetLoading(true));
    const state = yield select(makeSelectStore());

    const queryString = QueryString.stringify(query, { skipNull: true });
    const url = `${api.booksList}?${queryString || state.queryString}`;

    const { data } = yield call(request.get, url);

    yield put(
      storeMerge({
        records: data,
        isLoading: false,
        isUpdated: true,
        queryString,
      }),
    );
    yield call(onSuccess, data);
  } catch (err) {
    yield call(onError, err.message);
  } finally {
    yield put(storeSetLoading(false));
  }
}
