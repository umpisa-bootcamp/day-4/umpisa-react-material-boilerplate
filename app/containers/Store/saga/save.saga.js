import { call, put } from 'redux-saga/effects';
import QueryString from 'query-string';

import * as api from '../../../constants/api';
import * as request from '../../../utils/request';
import { storeSetLoading, storeMerge } from '../actions';

export default function* save(action) {
  const {
    params = {},
    query = {},
    onSuccess = () => {},
    onError = () => {},
  } = action.payload;

  try {
    yield put(storeSetLoading(true));
    const queryString = QueryString.stringify(query, { skipNull: true });
    const url = `${api.store}?${queryString}`;

    const { data } = yield call(request.post, url, params);

    yield put(
      storeMerge({
        item: data,
        isLoading: false,
        isUpdated: true,
      }),
    );
    yield call(onSuccess, data);
  } catch (err) {
    yield call(onError, err.message);
  } finally {
    yield put(storeSetLoading(false));
  }
}
