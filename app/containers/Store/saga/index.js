import { takeLatest } from 'redux-saga/effects';
import { STORE_FETCH, STORE_SAVE } from '../constants';
import fetch from './fetch.saga';
import save from './save.saga';

export default function* storeSaga() {
  yield takeLatest(STORE_FETCH, fetch);
  yield takeLatest(STORE_SAVE, save);
}
