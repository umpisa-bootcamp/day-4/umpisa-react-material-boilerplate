import { memo } from 'react';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { storeFetch, storeSave, storeMerge } from './actions';

import { makeSelectStore } from './selectors';

const mapStateToProps = createStructuredSelector({
  store: makeSelectStore(),
});

const mapDispatchToProps = {
  onStoreMerge: storeMerge,

  onStoreFetch: storeFetch,
  onStoreSave: storeSave,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
);
