/*
 *
 * Store reducer
 *
 */
import produce from 'immer';
import { merge } from '../../utils/object';
import { STORE_MERGE } from './constants';

export const initialState = {
  records: [],
  isLoading: false,
  isUpdated: false,
  error: '',
  message: '', // status messaging
  item: {},
  id: '', // active id for item
  queryString: '', // query string for api call
};

/* eslint-disable default-case, no-param-reassign */
const storeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case STORE_MERGE:
        return merge(draft, action.payload);
    }
    return draft;
  });

export default storeReducer;
