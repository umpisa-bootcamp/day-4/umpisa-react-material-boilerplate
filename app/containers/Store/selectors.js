import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the store state domain
 */

const selectStoreDomain = state => state.store || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Store
 */

const makeSelectStore = () =>
  createSelector(
    selectStoreDomain,
    substate => substate,
  );

export { selectStoreDomain, makeSelectStore };
