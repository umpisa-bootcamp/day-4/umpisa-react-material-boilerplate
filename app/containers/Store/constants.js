/*
 *
 * Store constants
 *
 */

export const STORE_MERGE = 'app/Store/STORE_MERGE';

export const STORE_FETCH = 'app/Store/STORE_FETCH';
export const STORE_SAVE = 'app/Store/STORE_SAVE';
