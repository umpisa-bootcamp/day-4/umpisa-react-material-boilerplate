import { createSelector } from 'reselect';
import { initialState } from './reducer';
import { makeSelectAuthUser, makeSelectAuthUserRole } from '../Auth/selectors';

/**
 * Direct selector to the adminDashboard state domain
 */

const selectAdminDashboardDomain = state =>
  state.adminDashboard || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by AdminDashboard
 */

const makeSelectAdminDashboard = () =>
  createSelector(
    selectAdminDashboardDomain,
    substate => substate,
  );

export {
  selectAdminDashboardDomain,
  makeSelectAdminDashboard,
  makeSelectAuthUser,
  makeSelectAuthUserRole,
};
