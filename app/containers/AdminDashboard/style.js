import { makeStyles } from '@material-ui/core';
import { grey1, red3, green1 } from '../../constants/color';

export const useStyles = makeStyles(() => ({
  panelTitle: {
    fontFamily: 'Roboto',
    fontSize: '14px',
    color: grey1,
  },
}));
