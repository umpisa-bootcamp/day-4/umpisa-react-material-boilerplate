import { call, put } from 'redux-saga/effects';

import * as api from '../../../constants/api';
import * as request from '../../../utils/request';
import { adminDashboardSetLoading, adminDashboardMerge } from '../actions';

export default function* save(action) {
  const { params, onSuccess = () => {}, onError = () => {} } = action.payload;

  try {
    yield put(adminDashboardSetLoading(true));

    const result = yield call(request.post, api.users, params);
    // store data on redux state
    yield put(
      adminDashboardMerge({
        records: result,
        isLoading: false,
        isUpdated: true,
      }),
    );
    yield call(onSuccess, result);
  } catch (err) {
    yield call(onError, err.message);
  } finally {
    yield put(adminDashboardSetLoading(false));
  }
}

export function* createNotification(action) {
  try {
    yield call(request.post, api.notifications, action.payload);
  } finally {
    yield put(adminDashboardSetLoading(false));
  }
}

export function* massUpload(action) {
  try {
    yield call(request.post, api.upload, action.payload);
  } finally {
    yield put(adminDashboardSetLoading(false));
  }
}
