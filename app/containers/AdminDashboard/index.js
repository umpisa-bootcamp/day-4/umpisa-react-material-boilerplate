/**
 *
 * Dashboard
 *
 */
import React, { useEffect /* useCallback */, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { omitBy, isNil } from 'lodash';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import composer from './composer';
import saga from './saga';
import { useStyles } from './style';

import * as routes from '../../constants/route';

import { getRecords } from '../../utils/helper/modelHelper';

export function Dashboard({ history, role, adminDashboard }) {
  useInjectSaga({ key: 'dashboard', saga });

  const classes = useStyles();

  return (
    <div>
      <Helmet>
        <title>Dashboard</title>
        <meta name="description" content="Description of Dashboard" />
      </Helmet>

      <h1 className={classes.panelTitle}>Dashboard</h1>
    </div>
  );
}

Dashboard.propTypes = {
  history: PropTypes.any,
  role: PropTypes.string,
  adminDashboard: PropTypes.any,
};

export default composer(Dashboard);
