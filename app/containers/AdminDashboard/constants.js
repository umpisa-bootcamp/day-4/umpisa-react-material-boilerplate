/*
 *
 * AdminDashboard constants
 *
 */

export const ADMIN_DASHBOARD_MERGE = 'app/AdminDashboard/ADMIN_DASHBOARD_MERGE';

export const ADMIN_DASHBOARD_FETCH = 'app/AdminDashboard/ADMIN_DASHBOARD_FETCH';
export const ADMIN_DASHBOARD_SAVE = 'app/AdminDashboard/ADMIN_DASHBOARD_SAVE';
