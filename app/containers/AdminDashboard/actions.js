/*
 *
 * AdminDashboard actions
 *
 */

import {
  ADMIN_DASHBOARD_MERGE,
  ADMIN_DASHBOARD_FETCH,
  ADMIN_DASHBOARD_SAVE,
} from './constants';

export function adminDashboardMerge(payload = {}) {
  return {
    type: ADMIN_DASHBOARD_MERGE,
    payload,
  };
}

export function adminDashboardFetch(payload = {}) {
  return {
    type: ADMIN_DASHBOARD_FETCH,
    payload,
  };
}

export function adminDashboardSave(payload = {}) {
  return {
    type: ADMIN_DASHBOARD_SAVE,
    payload,
  };
}

export function adminDashboardSetLoading(state = true) {
  return {
    type: ADMIN_DASHBOARD_MERGE,
    payload: {
      isLoading: state || false,
    },
  };
}

export function adminDashboardSetMessage(message = '') {
  return {
    type: ADMIN_DASHBOARD_MERGE,
    payload: { message },
  };
}
