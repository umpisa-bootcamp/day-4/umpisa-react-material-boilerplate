/*
 *
 * AdminDashboard reducer
 *
 */
import produce from 'immer';
import { merge } from '../../utils/object';
import { ADMIN_DASHBOARD_MERGE } from './constants';

export const initialState = {
  records: null,
  isLoading: true,
  isUpdated: false,
  error: '',
  message: '', // status messaging
};

/* eslint-disable default-case, no-param-reassign */
const adminDashboardReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case ADMIN_DASHBOARD_MERGE:
        return merge(draft, action.payload);
    }
    return draft;
  });

export default adminDashboardReducer;
