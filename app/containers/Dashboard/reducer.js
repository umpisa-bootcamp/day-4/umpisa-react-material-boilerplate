/*
 *
 * AdminDashboard reducer
 *
 */
import produce from 'immer';
import { merge } from '../../utils/object';
import { ADMIN_DASHBOARD_MERGE, ADMIN_DASHBOARD_ONCLICKROW } from './constants';

export const initialState = {
  book: {},
  records: [],
  isLoading: true,
  isUpdated: false,
  error: '',
  message: '', // status messaging
};

/* eslint-disable default-case, no-param-reassign */
const adminDashboardReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case ADMIN_DASHBOARD_MERGE:
        return merge(draft, action.payload);
      case ADMIN_DASHBOARD_ONCLICKROW:
        return merge(draft, action.payload);
    }
    return draft;
  });

export default adminDashboardReducer;
