/**
 *
 * Dashboard
 *
 */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
// import { omitBy, isNil } from 'lodash';

import { Grid, Button } from '@material-ui/core';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import composer from './composer';
import saga from './saga';
import reducer from './reducer';
import { useStyles } from './style';

// import * as routes from '../../constants/route';

import { getRecords } from '../../utils/helper/modelHelper';

import BooksTable from '../../components/BooksTable';

export function Dashboard({
  history,
  role,
  adminDashboard,
  onAdminDashboardFetch,
  onClickRow,
}) {
  useInjectSaga({ key: 'dashboard', saga });
  useInjectReducer({ key: 'dashboard', reducer });

  const { records } = adminDashboard;
  console.warn('rs', getRecords(adminDashboard, []));

  useEffect(() => {
    onAdminDashboardFetch({
      onSuccess: res => setBooks(res),
    });
  }, []);

  const [books, setBooks] = useState([]);

  useEffect(() => {
    setBooks(records);
  }, [records]);

  const classes = useStyles();

  const onAdminClickRow = book => {
    onClickRow({
      params: { book },
    });
  };

  return (
    <div>
      <Helmet>
        <title>Dashboard</title>
        <meta name="description" content="Description of Dashboard" />
      </Helmet>
      <Grid container>
        <Grid item md={6}>
          <Button onClick={() => history.push('/books/create')}>
            Create Book
          </Button>
          <BooksTable onClickRow={onAdminClickRow} books={books} />
        </Grid>
        <Grid item md={6} direction="column">
          <Grid>
            <h1>New Grid 1</h1>
          </Grid>
          <Grid>
            <h1>New Grid 2</h1>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

Dashboard.propTypes = {
  history: PropTypes.any,
  role: PropTypes.string,
  onAdminDashboardFetch: PropTypes.func.isRequired,
  adminDashboard: PropTypes.object.isRequired,
  onClickRow: PropTypes.func.isRequired,
};

export default composer(Dashboard);
