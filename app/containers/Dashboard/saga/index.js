import { takeLatest } from 'redux-saga/effects';
import {
  ADMIN_DASHBOARD_FETCH,
  ADMIN_DASHBOARD_SAVE,
  ADMIN_DASHBOARD_ONCLICKROW,
} from '../constants';
import fetch from './fetch.saga';
import save from './save.saga';
import selectBook from './selectBook.saga';

export default function* adminDashboardSaga() {
  yield takeLatest(ADMIN_DASHBOARD_FETCH, fetch);
  yield takeLatest(ADMIN_DASHBOARD_SAVE, save);
  yield takeLatest(ADMIN_DASHBOARD_ONCLICKROW, selectBook);
}
