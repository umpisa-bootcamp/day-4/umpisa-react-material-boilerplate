import { call, put } from 'redux-saga/effects';

import * as api from '../../../constants/api';
import * as request from '../../../utils/request';

import { adminDashboardSetLoading, adminDashboardMerge } from '../actions';

export default function* fetch(action) {
  const { onSuccess = () => {}, onError = () => {} } = action.payload;

  try {
    yield put(adminDashboardSetLoading(true));

    const result = yield call(request.get, api.booksList);
    // store data on redux state
    console.warn("result api call", result)
    yield put(
      adminDashboardMerge({
        records: result.data,
        isLoading: false,
        isUpdated: true,
      }),
    );
    yield call(onSuccess, result.data);
  } catch (err) {
    yield call(onError, err.message);
  } finally {
    yield put(adminDashboardSetLoading(false));
  }
}
