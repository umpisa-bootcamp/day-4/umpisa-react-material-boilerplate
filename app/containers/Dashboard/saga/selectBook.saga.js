import { call, put } from 'redux-saga/effects';
import { adminDashboardSetLoading, adminSelectBook } from '../actions';

export default function* selectBook(action) {
  const { params, onSuccess = () => {}, onError = () => {} } = action.payload;

  try {
    yield put(adminDashboardSetLoading(true));
    yield put(
      adminSelectBook({
        book: params,
        isLoading: false,
        isUpdated: true,
      }),
    );
    yield call(onSuccess, params);
  } catch (err) {
    yield call(onError, err.message);
  } finally {
    yield put(adminDashboardSetLoading(false));
  }
}
