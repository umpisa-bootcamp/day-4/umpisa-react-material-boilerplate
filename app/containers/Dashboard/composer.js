import { memo } from 'react';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import {
  adminDashboardFetch,
  adminDashboardSave,
  adminDashboardMerge,
  adminOnClickRow,
  adminSelectBook,
} from './actions';
import {
  makeSelectAdminDashboard,
  makeSelectAuthUser,
  makeSelectAuthUserRole,
} from './selectors';

const mapStateToProps = createStructuredSelector({
  adminDashboard: makeSelectAdminDashboard(),
  auth: makeSelectAuthUser(),
  role: makeSelectAuthUserRole(),
});

const mapDispatchToProps = {
  onAdminDashboardMerge: adminDashboardMerge,
  onAdminDashboardFetch: adminDashboardFetch,
  onAdminDashboardSave: adminDashboardSave,
  onClickRow: adminOnClickRow,
  onSelectBook: adminSelectBook,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
);
