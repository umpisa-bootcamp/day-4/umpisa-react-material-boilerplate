import { memo } from 'react';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { booksFormFetch, booksFormSave, booksFormMerge } from './actions';

import { makeSelectBooksForm } from './selectors';

const mapStateToProps = createStructuredSelector({
  booksForm: makeSelectBooksForm(),
});

const mapDispatchToProps = {
  onBooksFormMerge: booksFormMerge,

  onBooksFormFetch: booksFormFetch,
  onBooksFormSave: booksFormSave,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
);
