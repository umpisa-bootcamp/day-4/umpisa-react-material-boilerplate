/**
 *
 * Asynchronously loads the component for BooksForm
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
