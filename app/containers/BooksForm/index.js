/**
 *
 * BooksForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import reducer from './reducer';

import composer from './composer';

import saga from './saga';

import BooksFormInput from '../../components/BooksFormInput';

export function BooksForm({ onBooksFormSave, history }) {
  useInjectReducer({ key: 'booksForm', reducer });
  useInjectSaga({ key: 'booksForm', saga });

  const onSubmit = data => {
    onBooksFormSave({
      params: data,
      onSuccess: () => history.push('/'),
    });
  };

  return (
    <div>
      <Helmet>
        <title>BooksForm</title>
        <meta name="description" content="Description of BooksForm" />
      </Helmet>
      <h1>Books Form</h1>
      <BooksFormInput onSubmit={onSubmit} />
    </div>
  );
}

BooksForm.propTypes = {
  //  booksForm: PropTypes.object.isRequired,
  //  onBooksFormMerge: PropTypes.func.isRequired,
  //
  //  onBooksFormFetch: PropTypes.func.isRequired,
  history: PropTypes.object,
  onBooksFormSave: PropTypes.func.isRequired,
};

export default composer(BooksForm);
