/*
 *
 * BooksForm constants
 *
 */

export const BOOKS_FORM_MERGE = 'app/BooksForm/BOOKS_FORM_MERGE';

export const BOOKS_FORM_FETCH = 'app/BooksForm/BOOKS_FORM_FETCH';
export const BOOKS_FORM_SAVE = 'app/BooksForm/BOOKS_FORM_SAVE';
