import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the booksForm state domain
 */

const selectBooksFormDomain = state => state.booksForm || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by BooksForm
 */

const makeSelectBooksForm = () =>
  createSelector(
    selectBooksFormDomain,
    substate => substate,
  );

export { selectBooksFormDomain, makeSelectBooksForm };
