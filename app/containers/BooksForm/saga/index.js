import { takeLatest } from 'redux-saga/effects';
import { BOOKS_FORM_FETCH, BOOKS_FORM_SAVE } from '../constants';
import fetch from './fetch.saga';
import save from './save.saga';

export default function* booksFormSaga() {
  yield takeLatest(BOOKS_FORM_FETCH, fetch);
  yield takeLatest(BOOKS_FORM_SAVE, save);
}
