/*
 *
 * BooksForm actions
 *
 */

import {
  BOOKS_FORM_MERGE,
  BOOKS_FORM_FETCH,
  BOOKS_FORM_SAVE,
} from './constants';

export function booksFormMerge(payload = {}) {
  return {
    type: BOOKS_FORM_MERGE,
    payload,
  };
}

export function booksFormFetch(payload = {}) {
  return {
    type: BOOKS_FORM_FETCH,
    payload,
  };
}

export function booksFormSave(payload = {}) {
  return {
    type: BOOKS_FORM_SAVE,
    payload,
  };
}

export function booksFormSetLoading(state = true) {
  return {
    type: BOOKS_FORM_MERGE,
    payload: {
      isLoading: state || false,
    },
  };
}

export function booksFormSetMessage(message = '') {
  return {
    type: BOOKS_FORM_MERGE,
    payload: { message },
  };
}
