import { takeLatest, call } from 'redux-saga/effects';
import { AUTH_LOGOUT } from '../Auth/constants';
import { /* save, storageNames, */ remove } from '../../utils/persitedStore';
import history from '../../utils/history';
import { non } from '../../utils/function';

export function* logout() {
  try {
    yield remove();
    yield call(history.push, '/auth/signin');
  } catch (err) {
    non(err);
  }
}

export default function* authLogoutSaga() {
  yield takeLatest(AUTH_LOGOUT, logout);
}
