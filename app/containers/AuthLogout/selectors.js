import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the authLogout state domain
 */

const selectAuthLogoutDomain = state => state.authLogout || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by AuthLogout
 */

const makeSelectAuthLogout = () =>
  createSelector(
    selectAuthLogoutDomain,
    substate => substate,
  );

export default makeSelectAuthLogout;
export { selectAuthLogoutDomain };
