/**
 *
 * AuthLogout
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectAuthLogout from './selectors';
import reducer from './reducer';
import saga from './saga';
import { authLogout } from '../Auth/actions';

export function AuthLogout({ onAuthLogout }) {
  useInjectReducer({ key: 'authLogout', reducer });
  useInjectSaga({ key: 'authLogout', saga });

  useEffect(() => {
    onAuthLogout();
  }, []);

  return <div>logout</div>;
}

AuthLogout.propTypes = {
  onAuthLogout: PropTypes.func.isRequired, // redux
};

const mapStateToProps = createStructuredSelector({
  authLogout: makeSelectAuthLogout(),
});
const mapDispatchToProps = {
  onAuthLogout: authLogout,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(AuthLogout);
