/**
 *
 * Asynchronously loads the component for AuthLogout
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
