import { createSelector } from 'reselect';
import _ from 'lodash';
import { initialState } from './reducer';

import { userTypes } from '../../constants/userType';
import { createToken } from '../../utils/string';

/**
 * Direct selector to the auth state domain
 */

const selectAuthDomain = state => state.auth || initialState;

const makeSelectAuth = () =>
  createSelector(
    selectAuthDomain,
    substate => substate,
  );

const makeSelectAuthUser = () =>
  createSelector(
    selectAuthDomain,
    substate => substate.user,
  );

const makeSelectAuthUserToken = () =>
  createSelector(
    selectAuthDomain,
    substate => substate.token,
  );

const makeSelectAuthUserRole = () =>
  createSelector(
    selectAuthDomain,
    substate => {
      if (substate.user) {
        return substate.user.role;
      }
      return null;
    },
  );

const makeSelectIsLogged = () =>
  createSelector(
    selectAuthDomain,
    substate => {
      const expiresIn = _.get(substate, 'token.expiresIn', null);
      const exp = new Date(expiresIn);
      const now = new Date();
      // if (now.getTime() < exp.getTime()) {
      //   return substate.isLogged;
      // }
      // return false;
      return substate.isLogged;
    },
  );

const makeSelectIsAdmin = () =>
  createSelector(
    selectAuthDomain,
    substate => substate.user && Boolean(substate.user.isAdmin),
  );

const makeSelectUserType = () =>
  createSelector(
    selectAuthDomain,
    substate => {
      if (substate.user) {
        if (substate.user.role === userTypes.user) {
          return userTypes.user;
        }
        if (substate.user.role === userTypes.admin) {
          return userTypes.admin;
        }
      }
      // default user
      return userTypes.user;
    },
  );

const makeSelectAuthorization = () =>
  createSelector(
    selectAuthDomain,
    substate => createToken(substate.token),
  );

export default makeSelectAuth;
export {
  selectAuthDomain,
  makeSelectAuthUser,
  makeSelectAuthUserToken,
  makeSelectAuthUserRole,
  makeSelectIsLogged,
  makeSelectUserType,
  makeSelectIsAdmin,
  makeSelectAuthorization,
};
