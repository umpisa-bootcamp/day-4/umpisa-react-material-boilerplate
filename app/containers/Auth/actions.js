/*
 *
 * Auth actions
 *
 */

import {
  DEFAULT_ACTION,
  LOGIN,
  AUTH_LOGOUT,
  ERROR,
  SUCCESS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function authLogin(username, password, options) {
  return {
    type: LOGIN,
    payload: {
      email: username, // match to api call
      password,
      options,
    },
  };
}

export function authLogout(payload) {
  return {
    type: AUTH_LOGOUT,
    payload,
  };
}

export function authSuccess(payload) {
  return {
    type: SUCCESS,
    payload,
  };
}
export function authError(message) {
  return {
    type: ERROR,
    payload: message,
  };
}
