/*
 *
 * Auth constants
 *
 */

export const DEFAULT_ACTION = 'app/Auth/DEFAULT_ACTION';
export const LOGIN = 'app/Auth/LOGIN';

export const AUTH_LOGOUT = 'app/Auth/AUTH_LOGOUT';

export const ERROR = 'app/Auth/ERROR';
export const SUCCESS = 'app/Auth/SUCCESS';
