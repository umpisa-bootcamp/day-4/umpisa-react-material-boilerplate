/**
 *
 * Auth
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Switch, Route } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import makeSelectAuth from './selectors';
import saga from './saga';
import AuthSingin from '../AuthSingin';
import AuthLogout from '../AuthLogout';
import { authSignin, authLogout } from '../../constants/route';
import { useStyles } from './style';

export function Auth() {
  const classes = useStyles();
  // useInjectReducer({ key: 'auth', reducer }); // already on global reducer
  useInjectSaga({ key: 'auth', saga });

  return (
    <div className={classes.auth}>
      <Helmet>
        <title>Auth</title>
        <meta name="description" content="where session beggin" />
      </Helmet>
      {/* <FormattedMessage {...messages.header} /> */}
      <Switch>
        <Route path={authSignin} component={AuthSingin} />
        <Route path={authLogout} component={AuthLogout} />
      </Switch>
    </div>
  );
}
Auth.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeSelectAuth(),
});

function mapDispatchToProps() {
  return {
    // dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Auth);
