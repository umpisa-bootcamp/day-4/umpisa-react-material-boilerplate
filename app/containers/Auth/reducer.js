/*
 *
 * Auth reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  LOGIN,
  AUTH_LOGOUT,
  ERROR,
  SUCCESS,
} from './constants';

export const initialState = {
  token: {},
  user: {},
  isLogged: false,
};

/* eslint-disable default-case, no-param-reassign */
const authReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SUCCESS:
        draft.token = action.payload.tokens.access;
        draft.user = action.payload.user;
        draft.isLogged = true;
        break;
      case AUTH_LOGOUT:
        draft.token = null;
        draft.user = null;
        draft.isLogged = false;
        break;
      case LOGIN:
        break;
      case ERROR:
        break;
      case DEFAULT_ACTION:
        break;
    }
  });

export default authReducer;
