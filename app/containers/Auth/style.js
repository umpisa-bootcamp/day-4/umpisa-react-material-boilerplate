import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
  auth: {
    height: '100vh',
    backgroundColor: '#244E60',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));
