import { createMuiTheme } from '@material-ui/core';
import { themeColor } from './constants/color';

export const theme = createMuiTheme({
  spacing: 4,
  typography: {
    fontFamily: [
      // may bayad tong avenir
      // kapag wala pa sa system nya. san serif nlang tutal magmukha den lang
      // 'Avenir',
      // 'sans-serif',
      'Roboto',
      // 'Raleway',
      // 'Arial',
      // '-apple-system',
      // 'BlinkMacSystemFont',
      // '"Segoe UI"',
      // '"Helvetica Neue"',
      // '"Apple Color Emoji"',
      // '"Segoe UI Emoji"',
      // '"Segoe UI Symbol"',
    ].join(','),
  },
  palette: {
    primary: {
      main: '#2196F3',
      dark: themeColor,
    },
    success: {
      main: '#b7dfb9',
    },
    info: {
      main: '#a6d5fa',
    },
    warning: {
      main: '#ffd599',
    },

    // background: {
    //   // default: "#244E60",
    // }
  },
});
