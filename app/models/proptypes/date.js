import PropTypes from 'prop-types';

const dateProptype = PropTypes.oneOfType([PropTypes.string, PropTypes.number]);

export default dateProptype;
