export const authSignin = '/auth/signin';
export const authLogout = '/auth/logout';
export const splashScreen = '/';
export const adminScreen = '/admin';
export const dashboard = '/dashboard';
export const booksCreate = '/books/create';
export const booksForm = '/books/form';
export const home = '/home';
