import { makeStyles } from '@material-ui/core/styles';

export const constantStyles = makeStyles(() => ({
  displayCenter: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },

  spaceBetween: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));
