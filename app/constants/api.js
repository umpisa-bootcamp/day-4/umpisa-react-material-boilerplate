// api contants

const api = 'http://localhost:3001/v1';

export const url = api;
export const authLogin = `${url}/auth/login`;
export const booksList = `${url}/books`;
export const staticRoot = `${url}/../../public`;
export const store = `${url}/stores`;
