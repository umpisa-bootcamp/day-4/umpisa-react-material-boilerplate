export const white = '#ffffff';
export const black = '#000000';
export const themeColor = '#244E60';
export const dividerColor = '#1F4252';
export const grey = '#DADADA';
export const grey4 = '#9B9B9B';
export const darkGrey = 'rgba(0, 0, 0, 0.54)';
export const grey3 = '#828282';
export const grey2 = '#4F4F4F';
export const blue1 = '#2F80ED';

export const orange = '#F2994A';
export const grey1 = '#333333';
export const errorColor = '#F44336';
export const success = '#4BB543';
export const paleGrey = '#84A2AE';
export const red2 = '#E10050';
export const red3 = '#EB5757';
export const green1 = '#219653';
export const blue = '#2196F3';
export const grey5 = '#E0E0E0';

// new shades of grey
export const mediumGrey = '#C4C4C4';
export const lightGrey = '#F2F5F6';

// shades of yellow
export const mediumYellow = '#F2AB49';

// shades of red
export const mediumRed = '#EB2D4F';

// shades of green
export const mediumGreen = '#4BB543';
