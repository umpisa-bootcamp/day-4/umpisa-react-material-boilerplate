/* eslint-disable no-return-assign, no-param-reassign */
export const merge = (source, values) => {
  // handle object
  Object.keys(values).map(key => (source[key] = values[key]));
  return source;
};

export const getIdFromModel = (obj, d, idFiledName = '_id') => {
  if (!obj) {
    return d;
  }
  if (typeof obj === 'string') {
    // no need to deconstruct
    return obj;
  }
  if (obj[idFiledName]) {
    return obj[idFiledName];
  }
  return d;
};

export const arrayToObject = (arr = []) =>
  arr.reduce((p, c) => ({ ...p, [c._id]: c }), {});
