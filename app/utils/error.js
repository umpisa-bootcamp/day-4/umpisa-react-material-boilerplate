const GLOBAL_ERRORS = {
  'Failed to fetch': 'Connection error. Try again later',
};

export const getErrorMessage = customErrors => {
  const errorMap = { ...GLOBAL_ERRORS, ...customErrors };

  return e => {
    let errorMessage;
    if (e.response) {
      // currently the error struct of api is { code, message }
      errorMessage = errorMap[e.response.message];
    } else {
      // if error does not include a response object
      errorMessage = errorMap[e.message];
    }

    // if errors is not defined use this default
    if (!errorMessage) {
      return 'Something went wrong. Please try again later';
    }

    return errorMessage;
  };
};
