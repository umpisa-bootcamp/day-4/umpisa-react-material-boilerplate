import Axios from 'axios';
import { reduxStore } from '../configureStore';
import { makeSelectAuthUserToken } from '../containers/Auth/selectors';
import { createToken } from './string';

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  if (response.status === 204 || response.status === 205) {
    return null;
  }
  return response.json();
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
async function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = await response.json();
  throw error;
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export default function request(url, options) {
  return fetch(url, options)
    .then(checkStatus)
    .then(parseJSON);
}

const getHeader = async () =>
  makeSelectAuthUserToken()(await reduxStore.getState());

export const get = async url => {
  const headerToken = {};
  const userToken = await getHeader();
  if (userToken.accessToken && userToken.tokenType) {
    headerToken.Authorization = createToken(userToken);
  }

  // get token here
  return request(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      ...headerToken,
    },
  });
};

export const axios = async params => {
  const headerToken = {};
  const userToken = await getHeader();
  if (userToken.accessToken && userToken.tokenType) {
    headerToken.Authorization = createToken(userToken);
  }

  // get token here
  return Axios({
    method: 'get',
    responseType: 'json',
    data: {},
    headers: {
      'Content-Type': 'application/json',
      ...headerToken,
    },
    ...params,
  });
};

export const post = async (url, data = {}) => {
  const headerToken = {};
  const userToken = await getHeader();
  if (userToken.accessToken && userToken.tokenType) {
    headerToken.Authorization = createToken(userToken);
  }

  return request(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      ...headerToken,
    },
    body: JSON.stringify(data),
  });
};

export const uploadPost = async (url, data) => {
  const headerToken = {};
  const userToken = await getHeader();
  if (userToken.accessToken && userToken.tokenType) {
    headerToken.Authorization = createToken(userToken);
  }

  const formData = new FormData();
  formData.append('file', data);

  return axios({
    url,
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data',
      ...headerToken,
    },
    data: formData,
  });
};

export const put = async (url, data = {}) => {
  const headerToken = {};
  const userToken = await getHeader();
  if (userToken.accessToken && userToken.tokenType) {
    headerToken.Authorization = createToken(userToken);
  }

  return request(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      ...headerToken,
    },
    body: JSON.stringify(data),
  });
};

export const destroy = async (url, data = {}) => {
  const headerToken = {};
  const userToken = await getHeader();
  if (userToken.accessToken && userToken.tokenType) {
    headerToken.Authorization = createToken(userToken);
  }

  return request(url, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      ...headerToken,
    },
    body: JSON.stringify(data),
  });
};
