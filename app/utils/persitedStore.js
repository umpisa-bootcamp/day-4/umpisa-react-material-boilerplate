import { persistedStoreVersion } from '../constants/version';

export const storageNames = {
  auth: 'auth',
};

export const load = async key => {
  try {
    const result = await localStorage.getItem(
      `${persistedStoreVersion}-${key}`,
    );
    const data = JSON.parse(result);

    return data;
  } catch (e) {
    return null;
  }
};

export const save = async (key, data) => {
  await localStorage.setItem(
    `${persistedStoreVersion}-${key}`,
    JSON.stringify(data),
  );
};

export const remove = async () => {
  await localStorage.clear();
};
