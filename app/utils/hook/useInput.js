import { useState, useCallback } from 'react';

export const useInputState = (values = {}) => {
  const [state, setState] = useState(values);

  const onChange = useCallback(
    e => {
      setState({ ...state, [e.target.name]: e.target.value });
    },
    [state, setState],
  );

  const updateState = useCallback(
    (params = {}) => {
      setState({ ...state, ...params });
    },
    [state, setState],
  );

  return [state, onChange, setState, updateState];
};
