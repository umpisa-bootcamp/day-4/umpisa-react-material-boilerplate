export const toMoney = (val = 0, currency = '₱', space = true) => {
  if (!val) {
    return '0.00';
  }

  return `${currency}${space ? ' ' : ''}${Number(val)
    .toFixed(2)
    .replace(/\d(?=(\d{3})+\.)/g, '$&,')}`;
};

export const non = () => {};
export const nan = [];

export const getIds = (obj = []) =>
  !obj ? [] : obj.map(i => i._id).filter(i => Boolean(i));

export const snycTimeout = (i = 1000) =>
  new Promise(r => {
    setTimeout(() => {
      r();
    }, i);
  });
