import moment from 'moment';
import {
  dateFormat,
  shortReadableFormat,
  simpleFormat,
} from '../../constants/date';

export const formatMMYYDD = date => moment(date).format(dateFormat);

export const formatReadable = date => moment(date).format(shortReadableFormat);

export const formatSimple = date => moment(date).format(simpleFormat);
