export const getProp = (obj, prop, defaultVal = '') => {
  try {
    return obj[prop];
  } catch (e) {
    return defaultVal;
  }
};

export const getId = (obj, field) => {
  if (typeof obj[field] === 'string') {
    // unpopulated
    return obj[field];
  }
  // populated
  return (obj[field] || {})._id;
};

export const getRecords = (reduxState, defaultValue = {}) => {
  // this will give default value if has problem

  try {
    return reduxState ? reduxState.records || defaultValue : defaultValue;
  } catch (e) {
    return defaultValue;
  }
};

export const getPageTotal = (reduxState, defaultValue = 1) => {
  try {
    if (
      reduxState &&
      reduxState.records &&
      reduxState.records.pagination &&
      reduxState.records.pagination.totalPage
    ) {
      return reduxState.records.pagination.totalPage;
    }
    return defaultValue;
  } catch (e) {
    return defaultValue;
  }
};

export const getPaginationTotal = (reduxState, defaultValue = 1) => {
  try {
    if (
      reduxState &&
      reduxState.pagination &&
      reduxState.pagination.totalPage
    ) {
      return reduxState.pagination.totalPage;
    }
    return defaultValue;
  } catch (e) {
    return defaultValue;
  }
};

export const getIsLoading = (reduxState, defaultValue = false) => {
  try {
    return reduxState ? reduxState.isLoading || defaultValue : defaultValue;
  } catch (e) {
    return defaultValue;
  }
};
