import React, { lazy, Suspense } from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';

const loadable = (
  importFunc,
  { fallback } = {
    fallback: <LinearProgress />,
  },
) => {
  const LazyComponent = lazy(importFunc);

  return props => (
    <Suspense fallback={fallback}>
      <LazyComponent {...props} />
    </Suspense>
  );
};

export default loadable;
