import moment from 'moment';
import { dateFormat } from 'constants/date';

export const toDateFormat = time => moment(time).format(dateFormat);

export const formatDateColumn = date => {
  const isCurrentDate = moment(date).isSame(new Date(), 'day');

  if (isCurrentDate) {
    return `Today ${moment(date).format('hh:mm a')}`;
  }
  return moment(date).format('MM/DD/YYYY');
};
