export const toFullName = (
  { firstName = '', lastName = '' } = { firstName: '', lastName: '' },
) => {
  if (!firstName && !lastName) return 'No name';
  if (!firstName || !lastName)
    return `${capitalizeFirstLetter(lastName)} ${capitalizeFirstLetter(
      firstName,
    )}`;
  return `${capitalizeFirstLetter(lastName)}, ${capitalizeFirstLetter(
    firstName,
  )}`;
};

export const capitalizeFirstLetter = name =>
  name[0].toUpperCase() + name.substring(1);

export const createToken = userToken =>
  `${userToken.tokenType} ${userToken.accessToken}`;

export const pascalCase = str => str.charAt(0).toUpperCase() + str.substring(1);

export const truncateWithEllipses = (text, max, ellipses = '...') =>
  text.substr(0, max - 1) + (text.length > max ? ellipses : '');
