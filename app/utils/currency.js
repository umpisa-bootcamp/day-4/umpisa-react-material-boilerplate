import Decimal from 'decimal.js';

export const decimal = amount => new Decimal(amount);

export const formatCurrency = (amount = 0.0, currency = '₱') =>
  `${currency} ${Number(amount).toLocaleString('en-US')}`;

export const toFixed = num => num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
