import * as Firebase from 'firebase/app';
import 'firebase/auth';
import { firebaseConfig } from '../../constants/firebaseConfig';

Firebase.initializeApp(firebaseConfig);
export const firebase = Firebase;
